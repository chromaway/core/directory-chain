definitions:
  chain_version: &chain_version
    release_version: ${RELEASE_VERSION:-dev}
    release_commit: ${RELEASE_COMMIT:-dev}
blockchains:
  testnet:
    module: management_chain_testnet
    config:
      config_consensus_strategy: HEADER_HASH
      revolt:
        fast_revolt_status_timeout: 2000
        revolt_when_should_build_block: true
      signers:
        - x"02CCF3A609497F5C42C1A79433BD00221706772A9532C51DB1559559173DB87A8B"
      sync_ext:
        - "net.postchain.d1.icmf.IcmfReceiverSynchronizationInfrastructureExtension"
      gtx:
        modules:
          - "net.postchain.d1.icmf.IcmfSenderGTXModule"
          - "net.postchain.d1.icmf.IcmfReceiverGTXModule"
          - "net.postchain.eif.transaction.signerupdate.directorychain.SignerUpdateGTXModule"
          - "net.postchain.d1.iccf.IccfGTXModule"
      icmf:
        receiver:
          anchoring:
            topics:
              - G_configuration_updated
              - G_configuration_failed
              - G_last_anchored_heights
          global:
            topics:
              - G_create_cluster
              - G_create_container
              - G_upgrade_container
              - G_assign_subnode_image_to_container
              - G_stop_container
              - G_restart_container
              - G_remove_container
              - G_register_dapp_provider
              - G_change_dapp_providers_state
    moduleArgs:
      common.init:
        initial_provider: 02FEA5C0D8396B38C50200F2A583DCC8ED23416B9F9700A4AA435D57865939A536
        genesis_node:
          - 02CCF3A609497F5C42C1A79433BD00221706772A9532C51DB1559559173DB87A8B
          - node0.testnet.chromia.com
          - 9870
          - https://node0.testnet.chromia.com:7740
          - SE
      common:
        allow_blockchain_dependencies: false
        provider_quota_max_actions_per_day: 100
      common.queries:
        developers:
          - andrei.ursu@chromaway.com
          - eugene.tykulov@chromaway.com
          - johan.nilsson@chromaway.com
          - mikael.staldal@chromaway.com
          - robert.wideberg@chromaway.com
      proposal_blockchain_move:
        provider_quota_move_cost: 35
      proposal_blockchain.util:
        max_config_path_depth: 10
        max_config_size: 5242880 # 5 MiB
        max_block_size: 27262976 # 26 MiB
        min_inter_block_interval: 1000
        min_fast_revolt_status_timeout: 2000
        allowed_dapp_chain_gtx_modules:
          - "net.postchain.rell.module.RellPostchainModuleFactory"
          - "net.postchain.gtx.StandardOpsGTXModule"
          - "net.postchain.d1.icmf.IcmfSenderGTXModule"
          - "net.postchain.d1.icmf.IcmfReceiverGTXModule"
          - "net.postchain.d1.iccf.IccfGTXModule"
          - "net.postchain.eif.EifGTXModule"
          - "net.postchain.web.WebStaticGTXModuleFactory"
        allowed_dapp_chain_sync_exts:
          - "net.postchain.d1.icmf.IcmfReceiverSynchronizationInfrastructureExtension"
          - "net.postchain.eif.EifSynchronizationInfrastructureExtension"
        allowed_blockchain_features:
          - "merkle_hash_version"
        required_blockchain_features: []
        require_eif_snapshot_version: 2
      chain_version: *chain_version
      housekeeping:
        max_empty_container_time: 259200000 # 3 days
  system_anchoring:
    module: anchoring_chain_system
    config:
      config_consensus_strategy: HEADER_HASH
      blockstrategy:
        maxblocktime: 300000
      revolt:
        fast_revolt_status_timeout: 2000
        revolt_when_should_build_block: true
      sync_ext:
        - "net.postchain.d1.icmf.IcmfReceiverSynchronizationInfrastructureExtension"
      gtx:
        modules:
          - "net.postchain.d1.anchoring.system.SystemAnchoringGTXModule"
          - "net.postchain.d1.icmf.IcmfSenderGTXModule"
          - "net.postchain.d1.icmf.IcmfReceiverGTXModule"
      icmf:
        receiver:
          global:
            topics:
              - G_get_last_anchored_heights
    moduleArgs:
      chain_version: *chain_version
    test:
      modules:
        - anchoring_chain_system.test
  cluster_anchoring:
    module: anchoring_chain_cluster
    config:
      config_consensus_strategy: HEADER_HASH
      blockstrategy:
        maxblocktime: 300000
      revolt:
        fast_revolt_status_timeout: 2000
        revolt_when_should_build_block: true
      sync_ext:
        - "net.postchain.d1.icmf.IcmfReceiverSynchronizationInfrastructureExtension"
      gtx:
        modules:
          - "net.postchain.d1.anchoring.cluster.ClusterAnchoringGTXModule"
          - "net.postchain.d1.icmf.IcmfSenderGTXModule"
          - "net.postchain.d1.icmf.IcmfReceiverGTXModule"
      icmf:
        receiver:
          directory-chain:
            topics:
              - L_container_blockchain_update
          global:
            topics:
              - G_get_last_anchored_heights
    moduleArgs:
      chain_version: *chain_version
    test:
      modules:
        - anchoring_chain_cluster.test
      moduleArgs:
        anchoring_chain_cluster:
          imcf_headers_with_messages_query_limit: 3 # Good for testing limits
  economy_chain_testnet:
    module: economy_chain_testnet
    config:
      config_consensus_strategy: HEADER_HASH
      revolt:
        fast_revolt_status_timeout: 2000
        revolt_when_should_build_block: true
      sync_ext:
        - "net.postchain.d1.icmf.IcmfReceiverSynchronizationInfrastructureExtension"
      gtx:
        modules:
          - "net.postchain.d1.icmf.IcmfReceiverGTXModule"
          - "net.postchain.d1.icmf.IcmfSenderGTXModule"
          - 'net.postchain.eif.EifGTXModule'
          - "net.postchain.d1.iccf.IccfGTXModule"
      icmf:
        receiver:
          anchoring:
            topics:
              - G_node_availability_report
              - G_resource_usage_statistics
          directory-chain:
            topics:
              - L_create_cluster_error
              - L_ticket_container_result
              - L_cluster_update
              - L_provider_update
              - L_provider_auth_update
              - L_node_update
              - L_cluster_node_update
              - L_blockchain_rid_topic
              - L_subnode_image_update
              - L_cluster_subnode_image_update
          global:
            topics:
              - G_evm_transaction_submitter_cost_topic
              - G_token_price_changed
          local:
            - topic: L_evm_block_events
              bc-rid: x"BA320798BA35DD6E38F3DF24D241B7D94E5613899F64520DFB53D59793CDC5A3"
    moduleArgs:
      lib.hbridge:
        evm_read_offsets:
          "97": 10
          "11155111": 10
          "84532": 10
      lib.ft4.core.accounts:
        rate_limit:
          active: 1
          max_points: 20
          recovery_time: 5000
          points_at_account_creation: 1
      lib.ft4.core.auth:
        evm_signatures_authorized_operations:
          - eif.hbridge.link_evm_eoa_account
      lib.ft4.core.accounts.strategies.transfer:
        rules:
          - sender_blockchain:
              - "42415FEF744C07C49568FB41C06857D94C4662F3627F9B62D5A9C7BCE0CF73AE" # chr repl -c '("EVM",97,x"8e59d72e4DdA56F26963C6b8c77cA1959E9A74F0").hash()', based on BSC testnet token address
              - "146F8A4D265F604F3016E67B89F96123F9E6767C339E068F921E95BE332F302B" # chr repl -c '("EVM",11155111,x"106574Ff19BC975918443B6560c75D391632f166").hash()', based on Sepolia token address
              - "0A1FAEE86F208B20DEDB7B7F082961BF25C8A49F6D30C6C2F76A3311769C4069" # chr repl -c '("EVM",84532,x"1F11F131E0866AaaBfcCaaF350A6c33Abb0308b8").hash()', based on BASE Sepolia token address
            sender: "*"
            recipient: "*"
            asset:
              - name: "Chromia Test" # Should match `economy_chain_module_args.asset_name`
                min_amount: 10000000L # 10.0 tCHR
            timeout_days: 1
            strategy: "open"
          - sender_blockchain: "*"
            sender: "*"
            recipient: "*"
            asset:
              - name: "Chromia Test" # Should match `economy_chain_module_args.asset_name`
                min_amount: 10000000L # 10.0 tCHR
            timeout_days: 1
            strategy: "fee"
      lib.ft4.core.accounts.strategies.transfer.fee:
        asset:
          - name: "Chromia Test"  # Should match `economy_chain_module_args.asset_name`
            amount: 10000000L # 10.0 tCHR
        fee_account: x"433C49368E0300FA3E2ADF9E5C98AC8D52A9621459A193D7DAD638B165E03621" # pool account
      lib.auth:
        # TODO set proper pubkey for auth-server-ft4
        auth_pubkey: x"0317FF1ECF25B04026B6F9FA1960BA58070FB1CFC98519F7BC02D71ED1B00B1997"
      # *economy_chain_test_claim_tchr_args
      economy_chain_test_claim_tchr:
        asset_name: "Chromia Test"
        asset_symbol: "tCHR"
        asset_decimals: 6
        amount_to_mint: 1000000000
        test_chr_refill_limit_millis: 604800000 # 1 week
        account_creator_pubkey: x"036AF40CBAFA718B1AFBB8335E3315B8BD3821AB2AA891E5287521EEB6462AE2C5"
      economy_chain:
        # *economy_chain_module_args
        evm_bridges:
          - network_id: 97
            asset_address: "8e59d72e4DdA56F26963C6b8c77cA1959E9A74F0"
            bridge_address: "247b4200a8e8Db6878A2A5e4CDfbF84a50f5fFa8"
          - network_id: 11155111
            asset_address: "106574Ff19BC975918443B6560c75D391632f166"
            bridge_address: "1641900FF4E8931eFDDb0607FEe4E3D3C980435D"
          - network_id: 84532
            asset_address: "1F11F131E0866AaaBfcCaaF350A6c33Abb0308b8"
            bridge_address: "71bf30cDaec15885F13A9f3448d3A27C72f0d3eA"
        asset_name: "Chromia Test"
        asset_symbol: "tCHR"
        asset_decimals: 6
        asset_icon: "https://s3.eu-central-1.amazonaws.com/www.chromiadev.net/assets/tCHR.png"
        pool_amount_to_mint: 1000000000
        staking_withdrawal_delay_ms: 120000 # 2 min
        staking_delegation_delay_ms: 604800000
        staking_rewards_payout_interval_ms: 86400000 # 1 day
        staking_withdrawals_payout_interval_ms: 30000 # 60 seconds (approximately every other block)
        staking_rewards_payout_batch_size: 50
        staking_rewards_share: 0.1
        pool_refill_limit_ms: 86400000 # 1 day
        max_dapp_providers_per_lease: 2 # Good for testing
        max_bridge_leases_per_container: 10
        evm_transaction_submitters_bonus: 0.1
        container_removal_timeout: 1209600000 # 2 weeks
        token_rate_update_interval_ms: 3600000 # 1 hour
        staking_oracle_pubkey: x"03C096B5A11941348CCFD305FB2F57C674E49882889DA2973635A2BDBA042D49A8"
      economy_chain_test_mint_on_ras_staking:
        amount_to_mint: 1000000000
      economy_chain_test_auth_server:
        # TODO set proper pubkey for admin for test networks
        admin_pubkey: x"0317AC5B309A05B81A68F1A64321A3C6E6799E037C12892838682EBB9A620316A6"
      chain_version: *chain_version
docs:
  title: Directory chain
  footerMessage: © 2024 Chromia
  additionalContent:
    - module-docs.md
  sourceLink:
    remoteUrl: https://gitlab.com/chromaway/core/directory-chain/-/tree/dev/src
    remoteLineSuffix: "#L"

compile:
  rellVersion: 0.14.5
