module;

import lib.icmf.constants.*;
import provider_auth.model.*;

val create_cluster_topic = ICMF_TOPIC_GLOBAL_PREFIX + "create_cluster";
val create_cluster_error_topic = ICMF_TOPIC_LOCAL_PREFIX + "create_cluster_error";

val create_container_topic = ICMF_TOPIC_GLOBAL_PREFIX + "create_container";
val stop_container_topic = ICMF_TOPIC_GLOBAL_PREFIX + "stop_container";
val restart_container_topic = ICMF_TOPIC_GLOBAL_PREFIX + "restart_container";
val upgrade_container_topic = ICMF_TOPIC_GLOBAL_PREFIX + "upgrade_container";
val remove_container_topic = ICMF_TOPIC_GLOBAL_PREFIX + "remove_container";
val assign_subnode_image_to_container_topic = ICMF_TOPIC_GLOBAL_PREFIX + "assign_subnode_image_to_container";

val register_dapp_provider_topic = ICMF_TOPIC_GLOBAL_PREFIX + "register_dapp_provider";
val change_dapp_providers_state_topic = ICMF_TOPIC_GLOBAL_PREFIX + "change_dapp_providers_state";

val ticket_container_result_topic = ICMF_TOPIC_LOCAL_PREFIX + "ticket_container_result";
val cluster_update_topic = ICMF_TOPIC_LOCAL_PREFIX + "cluster_update";
val cluster_node_update_topic = ICMF_TOPIC_LOCAL_PREFIX + "cluster_node_update";
val provider_update_topic = ICMF_TOPIC_LOCAL_PREFIX + "provider_update";
val provider_auth_update_topic = ICMF_TOPIC_LOCAL_PREFIX + "provider_auth_update";
val node_update_topic = ICMF_TOPIC_LOCAL_PREFIX + "node_update";
val subnode_image_update_topic = ICMF_TOPIC_LOCAL_PREFIX + "subnode_image_update";
val cluster_subnode_image_update_topic = ICMF_TOPIC_LOCAL_PREFIX + "cluster_subnode_image_update";

struct create_cluster_message {
    name: text;
    governor_voter_set_name: text;
    voter_set_name: text;
    cluster_units: integer;
    extra_storage: integer;
    proposer_pubkey: byte_array;
}

struct create_cluster_error_message {
    name: text;
    error_message: text;
}

struct create_container_message {
    ticket_id: integer;
    provider_pubkey: pubkey;
    container_units: integer;
    extra_storage: integer;
    cluster_name: text;
    subnode_image_name: text;
}

struct upgrade_container_message {
    ticket_id: integer;
    container_name: text;
    container_units: integer;
    extra_storage: integer;
    cluster_name: text;
}

struct assign_subnode_image_to_container_message {
    ticket_id: integer;
    container_name: text;
    subnode_image_name: text;
}

struct stop_container_message {
    container_name: text;
}

struct restart_container_message {
    container_name: text;
}

struct remove_container_message {
    container_name: text;
    ticket_id: integer? = null;
}

struct ticket_container_result_message {
    ticket_id: integer;
    error_message: text?;
    container_name: text? = null;
    cluster_name: text? = null;
}

struct cluster_update_message {
    name;
    deleted: boolean;
    operational: boolean;
    cluster_units: integer;
    extra_storage: integer;
    anchoring_chain: byte_array?;
}

struct cluster_node_update_message {
    name?;
    pubkey;
    replica_node: boolean;
    deleted: boolean;
}

struct provider_update_message {
    pubkey;
    system: boolean;
    tier: text;
    active: boolean;
}

/**
 * The message sent from DC to EC for each provider key update. Each message will contain all provider keys
 * to let EC wipe and add.
 */
struct provider_auth_update_message {
    provider_pubkey: pubkey;
    key_roles: list<provider_auth_update_role>;
}

/**
 * Belongs to the provider_auth_update_message message to store all keys and threshold for a specific key role.
 */
struct provider_auth_update_role {
    role: provider_key_role;
    threshold: integer;
    keys: list<(pubkey: pubkey, active: boolean)>;
}

struct cluster_provider_update_message {
    pairs: list<(text, pubkey)>;
    deleted: boolean;
}

struct node_update_message {
    provider_pubkey: pubkey;
    pubkey;
    active: boolean;
    territory: text;
    cluster_units: integer;
    extra_storage: integer;
    deleted: boolean;
}

struct register_dapp_provider_message {
    container_name: text;
    new_provider: pubkey;
}

struct change_dapp_providers_state_message {
    providers: list<pubkey>;
    enable: boolean;
}

struct subnode_image_update_message {
    name;
    url: text;
    digest: text;
    subnode_image_type: text;
    owner: pubkey;
    description: text;
    active: boolean;
}

struct cluster_subnode_image_update_message {
    cluster: text;
    subnode_image: text;
    deleted: boolean;
}
