@test module;

import ^^.iccf.*;
import ^.helper_operations.*;
import ^.iccf_test_lib.*;

val dummy_source_chain = "source".hash();

val dummy_transaction = gtx_transaction(
    gtx_transaction_body(
        dummy_source_chain,
        [
            gtx_operation("dummy", [(1).to_gtv()])
        ],
        list<gtv>()
    ),
    list<gtv>()
);

function test_iccf_proof() {
    rell.test.tx().op(
        check_proof(dummy_transaction)
    ).run_must_fail("No proof operation present for TX");

    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction),
        check_proof(dummy_transaction)
    ).run();
}

function test_iccf_proof_signer_validation() {
    val signed_dummy_transaction = gtx_transaction(
        gtx_transaction_body(
            dummy_source_chain,
            [
                gtx_operation("dummy", [(1).to_gtv()])
            ],
            [rell.test.keypairs.bob.pub.to_gtv()]
        ),
        [x"".to_gtv()]
    );

    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, signed_dummy_transaction),
        check_proof(signed_dummy_transaction)
    ).sign(rell.test.keypairs.alice).run_must_fail("Proof TX has to be signed by all the signers that signed the source TX");

    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, signed_dummy_transaction),
        check_proof(signed_dummy_transaction)
    ).sign(rell.test.keypairs.bob).run();
}

function test_iccf_proof_timestamp_validation() {
    val tx_time = 1728982413892;

    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction, timestamp = tx_time),
        check_proof(dummy_transaction, min_timestamp = tx_time + 1)
    ).run_must_fail("Transaction is too old, confirmed at %s".format(tx_time));

    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction, timestamp = tx_time),
        check_proof(dummy_transaction, min_timestamp = tx_time)
    ).run();

    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction, timestamp = tx_time),
        check_proof(dummy_transaction, max_timestamp = tx_time - 1)
    ).run_must_fail("Transaction is too new, confirmed at %s".format(tx_time));

    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction, timestamp = tx_time),
        check_proof(dummy_transaction, max_timestamp = tx_time)
    ).run();
}

function test_unique_iccf_proof_validation() {
    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction),
        check_proof(dummy_transaction, enforce_unique = true)
    ).nop().run();

    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction),
        check_proof(dummy_transaction, enforce_unique = true)
    ).nop().run_must_fail("Transaction has already been processed");
}

function test_require_operation() {
    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction),
        check_operation(dummy_transaction, "non_dummy")
    ).run_must_fail("Operation with name non_dummy not found in transaction");

    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction),
        check_operation(dummy_transaction, "dummy")
    ).run();
}

function test_extract_args() {
    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction),
        extract_args(dummy_transaction, "dummy", [(0).to_gtv()])
    ).run_must_fail("Expected args did not match");

    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction),
        extract_args(dummy_transaction, "dummy", [(1).to_gtv()])
    ).run();
}

function test_source_blockchain_validation() {
    val another_chain = x"03";
    rell.test.tx().op(
        iccf_proof_for(dummy_source_chain, dummy_transaction),
        check_proof(dummy_transaction, valid_source_blockchain_rids = set([another_chain]))
    ).run_must_fail("%s is not a valid source blockchain".format(dummy_source_chain));
}
