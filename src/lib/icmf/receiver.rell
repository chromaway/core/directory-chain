module;

import ^.constants.*;

/**
 * Event structure for ICMF receiver topic updates.
 */
struct icmf_receiver_topic_event {
    /** True will replace existing topics (except the ones defined in the blockchain configuration) while false
     * will add new topics */
    replace: boolean = true;
    topics: list<icmf_receiver_topic>;
}

/**
 * Topic configuration topic to be used with the receiver_icmf_update_topics
 * function. Check ICMF configuration documentation for details.
 */
struct icmf_receiver_topic {

    /** Topic name, prefixed with either G_ or L_ depending on type */
    topic: text;

    /** Blockchain rid for sender, optional for global but required for local */
    bc_rid: byte_array?;

    /** Height to start read messages from */
    skip_to_height: integer = 0;
}

operation __icmf_message(sender: byte_array, topic: text, body: gtv) {
    receive_icmf_message(sender, topic, body);
}

@extendable function receive_icmf_message(sender: byte_array, topic: text, body: gtv) {}

/**
 * Manage ICMF receiver topics dynamically within the dapp. These topics will extend the blockchain ICMF receiver
 * configuration, but not modify it since it requires a blockchain configuration update.
 * 
 * @param topics A list of receiver topic configurations.
 * @param replace If true it will replace all existing topics (except the ones defined in the block configuration).
                  If false it will add the topics to existing configuration.
 */
function receiver_icmf_update_topics(topics: list<icmf_receiver_topic>, replace: boolean = true) {
    for (topic in topics) {
        require_valid_icmf_topic_name(topic.topic);
    }
    op_context.emit_event("icmf_receiver_topics", icmf_receiver_topic_event(topics, replace).to_gtv_pretty());
}
