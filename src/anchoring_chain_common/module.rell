module;

import messaging.configuration_update_message.*;

@log entity anchor_block {
    key blockchain_rid: byte_array, block_height: integer;
    index timestamp: integer;
    block_rid: byte_array;
    index blockchain_rid, block_rid;
    block_header: byte_array;
    witness: byte_array;
    anchoring_tx_op_index: integer; // For batch anchoring it's the index in the array of blocks
}

struct block_header {
    blockchain_rid: byte_array;
    previous_block_rid: byte_array;
    merkle_root_hash: byte_array;
    timestamp: integer;
    height: integer;
    dependencies: gtv; // Can be either GtvNull or GtvArray
    extra: map<text,gtv>;
}

struct anchoring_tx_with_op_index {
    tx_rid: byte_array;
    tx_data: byte_array;
    tx_op_index: integer;
}

struct block_to_anchor {
    block_rid: byte_array;
    block_header;
    witness: byte_array;
}

operation __anchor_block_header(block_rid: byte_array, header: gtv, witness: byte_array) {
    anchor_block_header(block_rid, header, witness);
}

function anchor_block_header(block_rid: byte_array, header: gtv, witness: byte_array) {
    val decoded_header = block_header.from_gtv(header);
    create anchor_block(
        blockchain_rid = decoded_header.blockchain_rid,
        block_height = decoded_header.height,
        timestamp = decoded_header.timestamp,
        block_rid = block_rid,
        block_header = header.to_bytes(),
        witness = witness,
        anchoring_tx_op_index = op_context.op_index
    );

    after_anchoring_block(block_rid, decoded_header);
}

operation __batch_anchor_block_header(blocks_to_anchor: list<block_to_anchor>) {
    val blocks = range(blocks_to_anchor.size()) @* {} (map_block_to_anchor_to_entity(blocks_to_anchor[$], $));
    create anchor_block(blocks);

    for (block_to_anchor in blocks_to_anchor) {
        after_anchoring_block(block_to_anchor.block_rid, block_to_anchor.block_header);
    }
}

function map_block_to_anchor_to_entity(block_to_anchor, batch_index: integer): struct<anchor_block> {
    return struct<anchor_block>(
        blockchain_rid = block_to_anchor.block_header.blockchain_rid,
        block_height = block_to_anchor.block_header.height,
        timestamp = block_to_anchor.block_header.timestamp,
        block_rid = block_to_anchor.block_rid,
        block_header = block_to_anchor.block_header.to_gtv().to_bytes(),
        witness = block_to_anchor.witness,
        anchoring_tx_op_index = batch_index,
        transaction = op_context.transaction
    );
}

query get_last_anchored_block(blockchain_rid: byte_array): struct<anchor_block>? {
    return anchor_block @? { blockchain_rid } (@omit @sort_desc .block_height, $.to_struct()) limit 1;
}

query get_anchored_block_at_height(blockchain_rid: byte_array, height: integer): struct<anchor_block>? {
    return anchor_block @? { blockchain_rid, .block_height == height } ($.to_struct());
}

query get_anchoring_transaction_for_block_rid(blockchain_rid: byte_array, block_rid: byte_array): anchoring_tx_with_op_index? {
    return anchor_block @? { blockchain_rid, block_rid } (anchoring_tx_with_op_index(
        tx_rid = .transaction.tx_rid,
        tx_data = .transaction.tx_data,
        tx_op_index = .anchoring_tx_op_index
    ));
}

query get_anchor_block_by_transaction_block_height(blockchain_rid: byte_array, transaction_block_height: integer): struct<anchor_block>? {
    return anchor_block @? {blockchain_rid, .transaction.block.block_height <= transaction_block_height} (@omit @sort_desc .block_height, $.to_struct()) limit 1;
}

query is_block_anchored(blockchain_rid: byte_array, block_rid: byte_array): boolean {
    return exists(anchor_block @? { blockchain_rid, block_rid });
}

@extendable function after_anchoring_block(block_rid: byte_array, decoded_header: block_header) {}
