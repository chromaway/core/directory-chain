
function internal_common_vote(pubkey, proposal: common_proposal, vote: boolean) {
    require(common_voter_set_member @? { proposal.voter_set, pubkey }, pubkey + " must be a member of the voter set");
    val common_vote = create common_vote(proposal, pubkey, vote);
    create common_vote_transaction(common_vote, op_context.transaction, op_context.op_index);
    log("vote '%s' added for proposal %s".format(if (vote) "Yes" else "No", proposal_str(proposal)));
    try_to_apply_proposal(proposal);
}

function try_to_apply_proposal(prop: common_proposal) {
    val prop_str = proposal_str(prop);
    val results = get_common_proposal_voting_results(prop.rowid);
    when (results.voting_result) {
        pending -> log("proposal is still under discussion:", prop_str);
        rejected -> {
            log("proposal rejected:", prop_str);
            prop.state = common_proposal_state.REJECTED;
            delete_common_proposal(prop);
        }
        approved -> {
            log("proposal approved:", prop_str);
            apply_or_delay_proposal(prop);
        }
    }
}

function apply_or_delay_proposal(prop: common_proposal) {
    val proposal_delay = get_scheduled_event_delay(delay_type.PROPOSAL, prop.rowid.to_integer()) ?: 0;
    if (proposal_delay > 0) {
        log("Proposal %s is delayed and will be applied in %s ms".format(prop.rowid, proposal_delay));
        prop.state = common_proposal_state.DELAYED;
        add_delayed_event(delay_type.PROPOSAL, proposal_delay, prop.rowid.to_integer());
        set_delayed_event_state(delay_type.PROPOSAL, prop.rowid.to_integer(), true);
    } else {
        try_call_apply_common_voting_result(prop);
    }
}

/**
 * Callback for delayed or scheduled proposals to run the proposals apply function.
 */
@extend(proposal_delayed_apply)
function common_proposal_delayed_apply(proposal_id: integer) {
    val prop = common_proposal @? { .rowid == rowid(proposal_id) };
    if (prop != null) {
        try_call_apply_common_voting_result(prop);
    }
}

function try_call_apply_common_voting_result(prop: common_proposal){
    if (try_call(apply_common_voting_result(prop, *))){
        prop.state = common_proposal_state.APPROVED;
    } else {
        prop.state = common_proposal_state.FAILED;
    }
    delete_common_proposal(prop);
}

@extendable function proposal_str(prop: common_proposal): text? = "%s:%d".format(prop.proposal_type, prop.rowid);
