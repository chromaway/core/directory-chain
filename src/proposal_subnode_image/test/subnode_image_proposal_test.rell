@test module;

import common.operations.*;
import common.queries.*;
import common.test.setup.*;
import common.test.util.*;
import model.*;
import proposal_subnode_image.*;
import proposal_container.*;
import proposal_provider.*;
import direct_cluster.*;

function test_propose_subnode_image() {
    setup_module();

    // 0. governance
    val sp1 = rell.test.keypairs.bob;
    val sp2 = rell.test.keypairs.trudy;
    val np = rell.test.keypairs.eve;

    rell.test.tx().op(
        register_provider(initial_provider.pub, sp1.pub, provider_tier.NODE_PROVIDER),
        register_provider(initial_provider.pub, sp2.pub, provider_tier.NODE_PROVIDER),
        register_provider(initial_provider.pub, np.pub, provider_tier.NODE_PROVIDER),
        propose_provider_is_system(initial_provider.pub, sp1.pub, true),
        propose_provider_is_system(initial_provider.pub, sp2.pub, true),
        propose_provider_state(initial_provider.pub, np.pub, true),
        create_cluster(initial_provider.pub, "cluster1", voter_sets.system_p, list()),
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp1);
    
    val cluster1 = require_cluster("cluster1");

    rell.test.tx().op(
        propose_subnode_image(np.pub, "subnode_image1", "url", "digest", subnode_image_type.COMMON, "", "", "")
    ).sign(np)
        .run_must_fail("must have system privileges");

    rell.test.tx().op(
        propose_subnode_image(initial_provider.pub, "subnode_image@1", "url", "digest", subnode_image_type.COMMON, "", "", "")
    ).sign(initial_provider)
        .run_must_fail(invalid_entity_message);

    rell.test.tx().op(
        propose_subnode_image(initial_provider.pub, "subnode_image1", "url", "digest", subnode_image_type.COMMON, "one", "", "")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    val subnode_image1 = get_subnode_image("subnode_image1");
    assert_true(subnode_image1.active);

    rell.test.tx().op(
        propose_subnode_image(initial_provider.pub, "subnode_image1", "url", "digest", subnode_image_type.COMMON, "", "", "")
    ).nop()
        .sign(initial_provider)
        .run_must_fail("Subnode image subnode_image1 already exists");

    rell.test.tx().op(
        propose_subnode_image(initial_provider.pub, "subnode_image2", "url2", "digest2", subnode_image_type.SPECIAL, "two", "", "")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    val subnode_image2 = get_subnode_image("subnode_image2");

    rell.test.tx().op(
        propose_subnode_image_state(initial_provider.pub, "subnode_image1", true)
    ).nop()
        .sign(initial_provider)
        .run_must_fail("Subnode image subnode_image1 already active");

    rell.test.tx().op(
        propose_subnode_image_state(initial_provider.pub, "subnode_image1", false)
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    val subnode_image1_2 = get_subnode_image("subnode_image1");
    assert_false(subnode_image1_2.active);

    rell.test.tx().op(
        propose_subnode_image_state(initial_provider.pub, "bogus", false)
    ).nop()
        .sign(initial_provider)
        .run_must_fail("Subnode image bogus not found");

    rell.test.tx().op(
        propose_subnode_image_state(initial_provider.pub, "subnode_image1", false)
    ).nop()
        .sign(initial_provider)
        .run_must_fail("Subnode image subnode_image1 already inactive");

    assert_true(get_cluster_subnode_images(cluster1.name).empty());

    rell.test.tx().op(
        propose_add_cluster_subnode_image(initial_provider.pub, "cluster1", "subnode_image2")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    assert_equals(get_cluster_subnode_images(cluster1.name), [
        (name="subnode_image2", url="url2", digest="digest2")
    ]);

    rell.test.tx().op(
        propose_remove_cluster_subnode_image(initial_provider.pub, "cluster1", "subnode_image2")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    assert_true(get_cluster_subnode_images(cluster1.name).empty());

    rell.test.tx().op(
        propose_remove_cluster_subnode_image(initial_provider.pub, "cluster1", "subnode_image2")
    ).nop().sign(initial_provider).run_must_fail("Subnode image subnode_image2 is not allowed by cluster cluster1");

    rell.test.tx().op(
        propose_update_subnode_image(initial_provider.pub, "subnode_image1", "url", "digest")
    ).sign(initial_provider).run_must_fail("Subnode image subnode_image1 already up-to-date");

    rell.test.tx().op(
        propose_update_subnode_image(initial_provider.pub, "subnode_image1", "url", "digest2")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    assert_equals(get_subnode_image("subnode_image1").digest, "digest2");

    assert_equals(get_subnode_images(), [
        subnode_image_data(
            name = "subnode_image1",
            url = "url",
            digest = "digest2",
            subnode_image_type = subnode_image_type.COMMON,
            owner = initial_provider.pub,
            active = false,
            description = "one",
        ),
        subnode_image_data(
            name = "subnode_image2",
            url = "url2",
            digest = "digest2",
            subnode_image_type = subnode_image_type.SPECIAL,
            owner = initial_provider.pub,
            active = true,
            description = "two",
        )
    ]);
}

function test_propose_container_with_subnode_image() {
    setup_module();

    // 0. governance
    val sp1 = rell.test.keypairs.bob;
    val sp2 = rell.test.keypairs.trudy;
    val np = rell.test.keypairs.eve;

    rell.test.tx().op(
        register_provider(initial_provider.pub, sp1.pub, provider_tier.NODE_PROVIDER),
        register_provider(initial_provider.pub, sp2.pub, provider_tier.NODE_PROVIDER),
        register_provider(initial_provider.pub, np.pub, provider_tier.NODE_PROVIDER),
        propose_provider_is_system(initial_provider.pub, sp1.pub, true),
        propose_provider_quota(initial_provider.pub, provider_tier.NODE_PROVIDER, provider_quota_type.max_containers, 10),
        propose_provider_is_system(initial_provider.pub, sp2.pub, true),
        propose_provider_state(initial_provider.pub, np.pub, true),
        create_cluster(initial_provider.pub, "cluster1", voter_sets.system_p, list()),
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp1);
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    rell.test.tx().op(
        propose_subnode_image(initial_provider.pub, "subnode_image1", "url", "digest", subnode_image_type.COMMON, "one", "", "")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    rell.test.tx().op(
        propose_subnode_image(initial_provider.pub, "subnode_image2", "url", "digest", subnode_image_type.SPECIAL, "two", "", "")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    rell.test.tx().op(
        propose_subnode_image(initial_provider.pub, "subnode_image3", "url", "digest", subnode_image_type.COMMON, "three", "", "")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    rell.test.tx().op(
        propose_subnode_image_state(initial_provider.pub, "subnode_image3", false)
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    rell.test.tx().op(
        propose_container_with_subnode_image(initial_provider.pub, "cluster1", "container1", voter_sets.system_p, "subnode_image1")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    assert_equals(get_container_data("container1").subnode_image, "subnode_image1");

    rell.test.tx().op(
        propose_container_with_subnode_image(initial_provider.pub, "cluster1", "container2", voter_sets.system_p, "subnode_image2")
    ).sign(initial_provider).run_must_fail("Subnode image subnode_image2 is not compatible with cluster cluster1");

    rell.test.tx().op(
        propose_add_cluster_subnode_image(initial_provider.pub, "cluster1", "subnode_image2")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    rell.test.tx().op(
        propose_container_with_subnode_image(initial_provider.pub, "cluster1", "container2", voter_sets.system_p, "subnode_image2")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    assert_equals(get_container_data("container1").subnode_image, "subnode_image1");

    rell.test.tx().op(
        propose_container_with_subnode_image(initial_provider.pub, "cluster1", "container3", voter_sets.system_p, "subnode_image3")
    ).sign(initial_provider).run_must_fail("Subnode image subnode_image3 is not active");

    rell.test.tx().op(
        propose_container(initial_provider.pub, "cluster1", "container3", voter_sets.system_p, "subnode_image1")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    assert_equals(get_container_data("container3").subnode_image, null);

    rell.test.tx().op(
        propose_container_subnode_image(initial_provider.pub, "container3", "subnode_image1")
    ).sign(initial_provider).run();
    _make_vote_v65(sp1);
    _make_vote_v65(sp2);

    assert_equals(get_container_data("container3").subnode_image, "subnode_image1");

    rell.test.tx().op(
        propose_container_subnode_image(initial_provider.pub, "container2", "subnode_image1")
    ).sign(initial_provider).run_must_fail("Container already has a subnode image");
}

function _make_vote_v65(voter: rell.test.keypair) {
    rell.test.tx().op(
        make_vote_v65(last_proposal().rowid.to_integer(), true)
    ).sign(voter).run();
}
