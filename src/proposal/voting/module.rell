module;

import ^.*;
import common_consensus.*;
import delay.proposal_callback.*;

entity vote {
    key proposal, provider;
    vote: boolean; // yes | no
}

/** To track the transaction of each vote */
entity vote_transaction {
    key vote;
    transaction;
    op_index: integer;
}

function create_voter_set_internal(name, threshold: integer = 0, governor: voter_set? = null) {
    val vs = create voter_set(name, threshold);
    if (exists(governor)) {
        create voter_set_governance(voter_set = vs, governor = governor);
    } else {
        create voter_set_governance(voter_set = vs, governor = vs);
    }
    return vs;
}

/**
 * Create a voter set.
 *
 * Governance semantics:
 * Anybody can create a new voter_set directly, but it consumes an action point.
 * voter_set can be updated. If voter_set_governance is defined, it will control voter set, otherwise, it is the voter set itself.
 *
 * Permission: any
 * 
 * Rate limit: actions
 * 
 * @param my_pubkey pubkey of provider
 */
operation create_voter_set(my_pubkey: pubkey, name, threshold: integer, initials: list<pubkey>?, governor: text?) {
    require_is_provider_with_rate_limit(my_pubkey);
    validate_entity_name(name);
    val governor_set = if (governor != null) require_voter_set(governor) else null;
    val vs = create_voter_set_internal(name, threshold, governor_set);
    if (exists(initials)) {
        for (prov in initials) {
            val p = require_provider(prov);
            create voter_set_member(voter_set = vs, p);
        }
    }
}


/**
 * Vote for proposal.
 *
 * Permission: voter set member
 *
 * Rate limit: no
 *
 * @param my_pubkey pubkey of provider
 */
operation make_vote(my_pubkey: pubkey, proposal_id: integer, vote: boolean) {
    make_vote_internal(proposal_id, vote);
}

/**
 * Vote for proposal.
 * 
 * Permission: voter set member
 * 
 * Rate limit: no
 */
operation make_vote_v65(proposal_id: integer, vote: boolean) {
    make_vote_internal(proposal_id, vote);
}

function make_vote_internal(proposal_id: integer, vote: boolean) {
    val prop = require_proposal(rowid(proposal_id));
    val provider = require_provider_signer_with_rate_limit();
    require(prop.state == proposal_state.PENDING, "The proposal is already %s".format(prop.state));
    require_voter_set_member(prop.voter_set, provider);
    require(empty(vote @? {prop, provider}), "Only one vote per pubkey is allowed");
    internal_vote(provider, prop, vote);
}

/**
 * Delete vote for proposal.
 *
 * Permission: voter set member
 *
 * Rate limit: no
 *
 * @param my_pubkey pubkey of provider
 */
operation retract_vote(my_pubkey: pubkey, proposal_id: integer) {
    retract_vote_internal(proposal_id);
}

/**
 * Delete vote for proposal.
 * 
 * Permission: voter set member
 * 
 * Rate limit: no
 */
operation retract_vote_v65(proposal_id: integer) {
    retract_vote_internal(proposal_id);
}

function retract_vote_internal(proposal_id: integer) {
    val prop = require_proposal(rowid(proposal_id));
    val provider = require_provider_signer_with_rate_limit();
    require(prop.state == proposal_state.PENDING, "The proposal is already %s".format(prop.state));
    require_voter_set_member(prop.voter_set, provider);

    val vote = vote @? { prop, provider };
    if (exists(vote)) {
        delete vote_transaction @? { vote };
        delete vote;
        log("vote for proposal retracted:", proposal_str(prop));
        try_to_apply_proposal(prop);
    } else {
        log("no vote for proposal to retract:", proposal_str(prop));
    }
}