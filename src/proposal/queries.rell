struct proposal_data {
    id: rowid;
    timestamp: integer;
    type: proposal_type;
    proposed_by: pubkey;
    description: text;
    state: proposal_state;

    /** If proposal is approved and the delay mechanism is enabled this will contain the time it will be applied */
    apply_at: timestamp?;

    /** The scheduled time to apply the proposal if it gets approved before this time */
    scheduled_at: timestamp?;

    /** Voter set name to control voting for this proposal */
    voter_set_name: text? = null;

    tx_rid: byte_array?;
}

/**
 * Returns proposals by criteria.
 */
query get_proposals_range(from: timestamp, until: timestamp, only_pending: boolean) {
    return proposal @* {
        .timestamp >= from,
        .timestamp <= until,
        not(only_pending) or .state == proposal_state.PENDING
    } (.rowid, .proposal_type, .state);
}

/**
 * Returns proposals by criteria.
 *
 * @param my_pubkey pubkey of provider
 */
query get_relevant_proposals(from: timestamp, until: timestamp, only_pending: boolean, my_pubkey: pubkey) {
    return (proposal, voter_set_member) @* {
        voter_set_member.voter_set == proposal.voter_set,
        voter_set_member.provider.pubkey == my_pubkey,
        proposal.timestamp >= from,
        proposal.timestamp <= until,
        not(only_pending) or proposal.state == proposal_state.PENDING
    } (proposal.rowid, proposal.proposal_type, proposal.state);
}

/**
 * Returns proposals by rowId.
 */
query get_proposal(id: rowid?): proposal_data? {
    val result = if (id == null) proposal @? {} (@sort_desc @omit .rowid, $) limit 1 else proposal @? { id };
    if (result == null) return null;
    return proposal_data(
        id = result.rowid,
        timestamp = result.timestamp,
        type = result.proposal_type,
        proposed_by = result.proposed_by.pubkey,
        description = result.description,
        state = result.state,
        apply_at = get_delayed_event_time(delay_type.PROPOSAL, result.rowid.to_integer()),
        scheduled_at = get_scheduled_event_time(delay_type.PROPOSAL, result.rowid.to_integer()),
        voter_set_name = result.voter_set.name,
        tx_rid = result.transaction.tx_rid,
    );
}

function require_proposal(rowid) = require(proposal @? { rowid }, "Proposal " + rowid + " not found");

function get_latest_proposal(rowid?, proposal_type) = if (rowid == null) proposal @ { proposal_type, proposal_state.PENDING } ( @max proposal ) else proposal @ { rowid };

struct proposal_voting_results {
    positive_votes: integer;
    negative_votes: integer;
    max_votes: integer;
    threshold: integer;
    voting_result;

    /** Voter set name to control voting for this proposal */
    voter_set_name: text? = null;
}

/**
 * Returns proposals voting results.
 */
query get_proposal_voting_results(rowid): proposal_voting_results {
    val proposal = require_proposal(rowid);
    require(proposal.state == proposal_state.PENDING, "This proposal is closed as %s. For info about voting use get_proposal_voter_info query.".format(proposal.state));
    val positive_votes = positive_votes(proposal);
    val negative_votes = negative_votes(proposal);
    val max_votes = max_votes(proposal.voter_set);
    val threshold = proposal.voter_set.threshold;
    val status = when (_compute_common_consensus_result(positive_votes, negative_votes, max_votes, threshold)) {
        rejected -> voting_result.rejected;
        approved -> voting_result.approved;
        else -> voting_result.pending;
    };
    return proposal_voting_results(positive_votes, negative_votes, max_votes, threshold, status, proposal.voter_set.name);
}

struct proposal_voter {
    provider: byte_array;
    provider_name: text;
    vote: boolean;

    /** Transaction rid of this vote */
    tx_rid: byte_array? = null;

    /** Operation index of this vote */
    op_index: integer? = null;
}

/**
 * Returns proposal voters info.
 */
query get_proposal_voter_info(rowid): list<proposal_voter> {
    val proposal = require_proposal(rowid);
    val all_votes = vote @* { proposal } ( .rowid, provider_pubkey = .provider.pubkey,
        provider_name = .provider.name, .vote );

    val votes_with_tx = vote_transaction @* { .vote.rowid in all_votes @ {} ( @list .rowid ) }
        ( vote_rowid = .vote.rowid, tx_rid = .transaction.tx_rid, .op_index );

    val votes = list<proposal_voter>();
    for (vote in all_votes) {
        votes.add(proposal_voter(
            provider = vote.provider_pubkey, vote.provider_name, vote.vote,
            tx_rid = votes_with_tx @? { .vote_rowid == vote.rowid } ( .tx_rid ),
            op_index = votes_with_tx @? { .vote_rowid == vote.rowid } ( .op_index ),
        ));
    }
    return votes;
}

struct blockchain_configuration_attempt_data {
    state: blockchain_configuration_update_state;
    applied_at_height: integer;
    blockchain_rid: byte_array?;
}

/**
 * Returns blockchain configuration update attempt by tx rid.
 */
query get_blockchain_configuration_update_attempt_state_by_tx_rid(tx_rid: byte_array): blockchain_configuration_attempt_data? {
    val attempt = blockchain_configuration_update_attempt @? { .proposal.transaction.tx_rid == tx_rid };
    if (attempt == null) return null;
    return blockchain_configuration_attempt_data(
        attempt.state,
        attempt.applied_at_height,
        if (attempt.blockchain_rid.empty()) null else attempt.blockchain_rid
    );
}

/**
 * Returns blockchain configuration update attempt by proposal.
 */
query get_blockchain_configuration_update_attempt_state_by_proposal(rowid): blockchain_configuration_attempt_data? {
    val attempt = blockchain_configuration_update_attempt @? { .proposal.rowid == rowid };
    if (attempt == null) return null;
    return blockchain_configuration_attempt_data(
        attempt.state,
        attempt.applied_at_height,
        if (attempt.blockchain_rid.empty()) null else attempt.blockchain_rid
    );
}

query get_proposal_ids_by_tx_rid(tx_rid: byte_array): list<rowid> {
    return proposal @* { .transaction.tx_rid == tx_rid } (.rowid);
}
