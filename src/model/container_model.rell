enum container_resource_limit_type {
    container_units,
    max_blockchains,
    extra_storage
}

enum container_state {
    RUNNING, STOPPED, MIGRATING
}

entity container {
    key name;
    index cluster;
    index deployer: voter_set; // who can deploy and update bcs in this container
    proposed_by: provider;
    system: boolean = false;
    mutable latest_bc_removal: timestamp = if (op_context.exists) op_context.last_block_time else -1;
    mutable state: container_state = container_state.RUNNING;
}

entity container_resource_limit {
    key container, container_resource_limit_type;
    mutable value: integer;
}

//Defines which container a blockchain belongs to
entity container_blockchain { 
    key container, blockchain; 
}

function create_container_blockchain(container, blockchain) {
    create container_blockchain(container, blockchain);
    after_create_container_blockchain(container, blockchain);
}

@extendable function after_create_container_blockchain(container, blockchain) {}

function delete_container_blockchain(container_blockchain) {
    before_delete_container_blockchain(container_blockchain);
    delete container_blockchain;
}

@extendable function before_delete_container_blockchain(container_blockchain) {}

function delete_container_blockchain_by_blockchain(blockchain) {
    before_delete_container_blockchain_by_blockchain(container_blockchain @* { blockchain });
    delete container_blockchain @* { blockchain };
}

@extendable function before_delete_container_blockchain_by_blockchain(container_blockchain_list: list<container_blockchain>) {}

object standard_container_unit {
    mutable cpu: integer = 50; // Percent of cpus, i.e. 50 = 0.5 vCPU
    mutable ram: integer = 2048; // MiB
    mutable io_read: integer = 25; // MiB/s
    mutable io_write: integer = 20; // MiB/s
    mutable storage: integer = 16384; // MiB
}

object standard_container_defaults {
    mutable container_units: integer = 1;
    mutable max_blockchains: integer = 10;
    mutable extra_storage: integer = 0;
}

object system_container_defaults {
    mutable container_units: integer = 4;
    mutable max_blockchains: integer = -1;
    mutable extra_storage: integer = 0;
}

function empty_container_resource_limits() = map<container_resource_limit_type, integer>();
