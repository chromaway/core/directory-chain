module;

import common_proposal.*;
import common_proposal.voter_set_proposal.*;
import common_vault_dapp_details.*;
import common_helpers.constants.*;
import common_helpers.proposals.*;
import lib.iccf.*;
import lib.icmf.*;
import lib.icmf.receiver.*;

namespace eif {
    import dynamic_config: lib.eif.dynamic_config_common;
    import messaging: lib.eif.messaging;
    import lib.hbridge;
}

namespace ft4 {
    import lib.ft4.accounts;
    import lib.ft4.assets;
    import lib.ft4.auth;
    import lib.ft4.utils;
    import lib.ft4.accounts.strategies;
    import lib.ft4.accounts.strategies.transfer;
    import lib.ft4.accounts.strategies.transfer.fee;
    import lib.ft4.accounts.strategies.transfer.open;
    import lib.ft4.crosschain;
}

struct module_args {
    init_pubkey: pubkey;
    asset_name: text;
    asset_symbol: text;
    asset_icon: text;
    asset_decimals: integer;
    economy_chain_blockchain_rid: pubkey;
    proposal_revoke_timeout_days: integer;
}

object state {
    mutable initialized: boolean = false;
}

object token_chain_constants {
    /** The fee for creating a token */
    mutable listing_fee: integer = 100000000; // 100.0 CHR

    /** The fee for adding a bridge */
    mutable bridge_fee: integer = 100000000; // 100.0 CHR
}

val token_chain_governance_voter_set = "Token chain governance";
val token_chain_governance_account_type = "TOKEN_CHAIN_GOVERNANCE";
val pool_account_type = "TOKEN_CHAIN_POOL";

struct minting_policy {
    minters: set<byte_array>;
    /** 0 to disable */
    max_supply: big_integer;
    minting_interval_ms: integer;
    minting_amount: big_integer;
    accumulating_amount: boolean;
}

entity token {
    owner: ft4.accounts.account;
    key asset: ft4.assets.asset;
    creation_time: timestamp;
}

entity token_account_creation_brid {
    index token;
    blockchain_rid: byte_array;
}

entity token_account_minting_policy {
    index token;
    max_supply: big_integer;
    minting_interval_ms: integer;
    minting_amount: big_integer;
    accumulating_amount: boolean;
}

entity token_account_minting_policy_account {
    key token_account_minting_policy, ft4.accounts.account;
}

/**
 * Initializes the token chain
 */
operation init_token_chain() {
    require(state.initialized == false, "Token chain is already initialized");
    state.initialized = true;

    require(op_context.is_signer(chain_context.args.init_pubkey), "Operation must be signed by initial key");
    create_pool_account();
    register_assets();

    create_token_chain_governance_account();
    create_governance_voter_set();
}

function create_governance_voter_set() {
    val voter_set = create_common_voter_set_internal(
        token_chain_governance_voter_set,
        vote_type = common_proposal_vote_type.VOTER_SET_MEMBER_KEY
    );
    create common_voter_set_member ( voter_set, chain_context.args.init_pubkey );
}

function register_assets() {
    return ft4.crosschain.Unsafe.register_crosschain_asset(
            asset_id = get_asset_id(),
            name = chain_context.args.asset_name,
            symbol = chain_context.args.asset_symbol,
            decimals = chain_context.args.asset_decimals,
            issuing_blockchain_rid = chain_context.args.economy_chain_blockchain_rid,
            icon_url = chain_context.args.asset_icon,
            type = ft4.assets.ASSET_TYPE_FT4,
            uniqueness_resolver = x"",
            origin_blockchain_rid = chain_context.args.economy_chain_blockchain_rid,
    );
}

function create_pool_account() =
    ft4.accounts.create_account_without_auth(get_pool_account_id(), pool_account_type);

function get_pool_account_id() = pool_account_type.hash();

function get_pool_account() = ft4.accounts.account_by_id(get_pool_account_id());

function get_token_chain_governance_account_id() = token_chain_governance_account_type.hash();

function create_token_chain_governance_account() =
    ft4
    .accounts
    .create_account_without_auth(
        get_token_chain_governance_account_id(),
        token_chain_governance_account_type
    );

function get_token_chain_governance_account() = ft4.accounts.account_by_id(get_token_chain_governance_account_id());

function get_asset_id() = (chain_context.args.asset_name, chain_context.args.economy_chain_blockchain_rid).hash();

function get_asset(): ft4.assets.asset {
    val asset_id = get_asset_id();
    return ft4.assets.asset @ { .id == asset_id };
}

struct token_data {
    owner: byte_array;
    creation_time: timestamp;
    minting_policy: list<minting_policy>;
    account_creation_brids: list<byte_array>;
}

function require_token(ft4.assets.asset) = require(token @? { asset }, "No token for asset id found");

/**
 * @param asset_id The FT4 asset id of the token
 * @return Information about the token
 */
query get_token_info(asset_id: byte_array) {
    val asset = ft4.assets.Asset(asset_id);
    val token = require_token(asset);

    val token_account_minting_policies = token_account_minting_policy @* { token };
    val minting_policies = list<minting_policy>();
    for (minting_policy in token_account_minting_policies) {
        val minters = set<byte_array>();
        for (minter in token_account_minting_policy_account @* { minting_policy }) {
            minters.add(minter.account.id);
        }

        minting_policies.add(minting_policy(
            minters = minters,
            max_supply = minting_policy.max_supply,
            minting_interval_ms = minting_policy.minting_interval_ms,
            minting_amount = minting_policy.minting_amount,
            accumulating_amount = minting_policy.accumulating_amount,
        ));
    }

    return token_data(
        token.owner.id,
        token.creation_time,
        minting_policies,
        token_account_creation_brid @* { token } ( .blockchain_rid )
    );
}

@extend(receive_icmf_message) function icmf_message_receive(sender: byte_array, topic: text, body: gtv) {

    when (topic) {
        eif.messaging.evm_block_events_topic -> eif.messaging.handle_evm_block(eif.messaging.evm_block_message.from_gtv(body));
        else -> log("Unexpected ICMF topic %s".format(topic));
    }
}

function require_initialized() {
    require(state.initialized == true, "Token chain is not initialized");
}

function require_governance_voter_set() =
    require_common_voter_set(token_chain_governance_voter_set);

function require_is_governor() {
    return require(common_voter_set_member @? {
        require_governance_voter_set(),
        .pubkey in op_context.get_signers()
    } ( .pubkey ) limit 1, "Operation must be signed to member of token chain governor voter set" );
}

@extend(vault_dapp_details) function tc_vault_dapp_details() = vault_dapp_details_data(
    name = "Chromia Token Chain",
    description = "Token chain handles native tokens and EVM bridges",
    icon_media_url = "https://assets.chromia.com/chr.png",
    other_media_url = "https://assets.chromia.com/chr-1000.png",
);

@extend(validate_voter_set_proposal) function common_voter_set_update_allowed_ec(pending_proposal: voter_set_update.pending_proposal) {
    require(pending_proposal.voter_set.name == token_chain_governance_voter_set,
        "%s it the only voter set in token chain".format(token_chain_governance_voter_set));
}

