@test module;

import ^^.*;
import ^.helper_operations.*;

function test_find_dapp_details_with_all_media() {
    val rowid = rowid(1);
    val dapp_details = find_dapp_details(rowid, [dapp_content_type.icon, dapp_content_type.landscape, dapp_content_type.portrait, dapp_content_type.promotional, dapp_content_type.video]);

    val expected = (
        rowid = rowid,
        name = "Chromia Token Chain",
        description = "Token chain handles native tokens and EVM bridges",
        launch_url = null,
        genre = "System Chain",
        chain_list = [dapp_details_blockchain(
        brid = chain_context.blockchain_rid,
        name = "Chromia Token Chain",
        role = ""
    )],
        content = [
        dapp_media(name = "icon1", url = "https://assets.chromia.com/chr.png", dapp_content_type.icon),
        dapp_media(name = "landscape1", url = "https://assets.chromia.com/chr-1000.png", dapp_content_type.landscape),
        dapp_media(name = "portrait1", url = "https://assets.chromia.com/chr-1000.png", dapp_content_type.portrait),
        dapp_media(name = "promotional1", url = "https://assets.chromia.com/chr-1000.png", dapp_content_type.promotional),
    ]
    ).to_gtv_pretty();

    assert_equals(dapp_details, expected);
}

function test_require_is_governor() {

    rell.test.tx().op(require_is_governor_op())
        .run_must_fail("Voter set Token chain governance does not exist");

    rell.test.tx().op(
        test_init_token_chain()
    ).sign(rell.test.keypairs.alice).run();

    rell.test.tx().op(require_is_governor_op())
        .run_must_fail("Operation must be signed to member of token chain governor voter set");

    rell.test.tx().op(require_is_governor_op())
        .sign(rell.test.keypairs.bob)
        .run_must_fail("Operation must be signed to member of token chain governor voter set");

    rell.test.tx().op(require_is_governor_op())
        .sign(rell.test.keypairs.alice)
        .run();
}
