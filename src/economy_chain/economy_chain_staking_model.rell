
// It might be better for this to live in a table
enum network {
    BSC, ETH, CHR
}

object _staking_state {
    mutable last_reward_distribution: integer = 0;
    mutable last_withdrawal_distribution: integer = 0;
    mutable reward_first_distribution_block: integer = 0;
    mutable reward_distribution_batches: integer = 1;
}

entity staker_withdrawal_request {
    index finish_at: timestamp;
    index staker_state;
    index mutable active: boolean;
}

entity staker_state {
    mutable ft4.accounts.account;
    key ft4.accounts.account, network;
    mutable balance: integer;
    mutable last_update: timestamp;
    mutable pending_token_time: integer;
}

entity staker_state_to_provider {
    /** The time from when the delegation is active */
    index timestamp;
    index staker_state;
    provider;
    previous_provider: provider = provider @ { get_null_account() };
}
entity staking_payment {
    index timestamp;
    amount: integer;
    ft4.accounts.account;
    shared_amount: integer = 0;
    provider_pubkey: byte_array = x'';
}

entity pending_shared_payment {
    index ft4.accounts.account;
    amount: integer;
    provider_pubkey: byte_array;
}

// @dev If this is changed so should `_update_pending_token_time_all` and the query `staking_rewards_claimable_for`.
// @dev Please pay attention to the bits marked with ***.
function _update_pending_token_time(staker_state) {
    update (
        // Take all the states
        s: staker_state,
        // And attach to them the provider record
        p: staker_state_to_provider
    ) @* {
            s == staker_state // *** For this staker state. Only want to do one. ***
        and s == p.staker_state
            // Is old enough to be active
        and p.timestamp <= _staking_last_known_time()
        and empty(
            // And for which there is not also a state that meets that criteria...
            (pp: staker_state_to_provider) @* {
                pp.staker_state == s
                and pp.timestamp <= _staking_last_known_time()
                // ... and is is newer
                and (
                        pp.timestamp > p.timestamp
                    or  (pp.rowid > p.rowid and pp.timestamp == p.timestamp)
                )
            }
        )
    } (
        pending_token_time += (s.balance / units_per_asset) *
        max(0, // No less than zero time
            // We would like to start at ... 
            when(p.provider.pubkey) {
                // ... if we are transitioning to undelegated, the later of the undelegation time and now
                get_null_account() -> min(_staking_last_known_time(), p.timestamp);
                // ... or just the current time
                else -> _staking_last_known_time();
            }
            // Start at the greatest of whole system start time or that for this stake
            - max(
                staking_get_rewards_start(),
                when(p.previous_provider.pubkey) {
                    // If we undelegating here, the more recent of the last updated or the undelegation time
                    get_null_account() -> max(p.timestamp, s.last_update);
                    // ... if we don't unstake then just the last update time
                    else -> s.last_update;
                },
            )
        ),
        last_update = _staking_last_known_time()
    );
}

// @dev If this is changed so should `_update_pending_token_time` and the query `staking_rewards_claimable_for`.
// @dev Please pay attention to the bits marked with ***.
function _update_pending_token_time_all() {
    update (
        // Take all the states
        s: staker_state,
        // And attach to them the provider record
        p: staker_state_to_provider
    // For this staker state
    ) @* {
            // There no staker_state restriction here. We want them all.
            s == p.staker_state
            // Is old enough to be active
        and p.timestamp <= _staking_last_known_time()
        // *** Note we're taking all of them ***
        and empty(
            // And for which there is not also a state that meets that criteria...
            (pp: staker_state_to_provider) @* {
                pp.staker_state == s
                and pp.timestamp <= _staking_last_known_time()
                // ... and is is newer
                and (
                        pp.timestamp > p.timestamp
                    or  (pp.rowid > p.rowid and pp.timestamp == p.timestamp)
                )
            }
        )
    } (
        pending_token_time += (s.balance / units_per_asset) *
        max(0, // No less than zero time
            // We would like to start at ... 
            when(p.provider.pubkey) {
                // ... if we are transitioning to undelegated, the later of the undelegation time and now
                get_null_account() -> min(_staking_last_known_time(), p.timestamp);
                // ... or just the current time
                else -> _staking_last_known_time();
            }
            // Start at the greatest of whole system start time or that for this stake
            - max(
                staking_get_rewards_start(),
                when(p.previous_provider.pubkey) {
                    // If we undelegating here, the more recent of the last updated or the undelegation time
                    get_null_account() -> max(p.timestamp, s.last_update);
                    // ... if we don't unstake then just the last update time
                    else -> s.last_update;
                },
            )
        ),
        last_update = _staking_last_known_time()
    );
}