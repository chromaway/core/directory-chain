@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(register_dapp_provider).mount_name,
    flags = ["T"],
    message = register_dapp_provider_auth_message(*)
);

function register_dapp_provider_auth_message(gtv) {
    val args = struct<register_dapp_provider>.from_gtv(gtv);
    return "Please sign the message\nto add dapp provider to container lease %s\nwith public key %s\n".format(
        args.container_name,
        args.new_provider
    );
}

/**
 * Register a new dapp provider, will be proposed to be added to the container deployer voter set. Must be called by container lease owner.
 *
 * @param container_name  container name
 * @param new_provider  pubkey of provider to register
 */
operation register_dapp_provider(container_name: text, new_provider: pubkey) {
    val lease = require_lease(container_name);
    require(not lease.expired, "Container lease is expired");

    val account = ft4.auth.authenticate();
    require(lease.account == account, "Lease for container %s was not created by you".format(container_name));

    // Some validation to minimize the risk of failing provider creation in directory chain
    validate_provider(new_provider);
    require(empty(lease_dapp_provider @? { .provider == new_provider }), "Provider has already been added");

    require(
        lease_dapp_provider @ { lease } (@sum 1) < chain_context.args.max_dapp_providers_per_lease,
        "No more dapp providers can be added to lease, maximum limit %s exceeded".format(chain_context.args.max_dapp_providers_per_lease)
    );

    create lease_dapp_provider(lease, new_provider);

    // Fire and forget, not super critical since no payment is made, client can query directory chain for progress
    send_message(register_dapp_provider_topic, register_dapp_provider_message(
        container_name,
        new_provider
    ).to_gtv());
}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(register_dapp_provider_with_payment).mount_name,
    flags = ["T"],
    message = register_dapp_provider_with_payment_auth_message(*)
);

function register_dapp_provider_with_payment_auth_message(gtv) {
    val args = struct<register_dapp_provider_with_payment>.from_gtv(gtv);
    return "Please sign the message\nto add dapp provider with public key %s\ncosting %s %s".format(
        args.new_provider,
        ft4.assets.format_amount_with_decimals(chain_context.args.dapp_provider_creation_cost, chain_context.args.asset_decimals),
        chain_context.args.asset_symbol
    );
}

/**
 * Register a new dapp provider for a fee.
 *
 * @param new_provider  pubkey of provider to register
 */
operation register_dapp_provider_with_payment(new_provider: pubkey) {
    val account = ft4.auth.authenticate();

    validate_provider(new_provider);

    ft4.assets.Unsafe.transfer(account, get_pool_account(), get_asset(), chain_context.args.dapp_provider_creation_cost);

    send_message(register_dapp_provider_topic, register_dapp_provider_message(
        "", // Empty string means that directory chain wont create a proposal to add this provider to a container voter set
        new_provider
    ).to_gtv());
}

function validate_provider(new_provider: pubkey) {
    require(empty(provider @? { .pubkey == new_provider }), "Another provider is already registered with the same public key");
    require(empty(node @? { .pubkey == new_provider }), "A node is already registered with the same public key");
}

@extend(on_lease_expired) function disable_lease_providers(lease) {
    change_lease_providers_state(lease, false);
}

@extend(on_lease_resurrected) function enable_lease_providers(lease) {
    change_lease_providers_state(lease, true);
}

@extend(on_expired_lease_removed) function remove_already_expired_lease_providers(lease) {
    delete lease_dapp_provider @* { lease };
}

function change_lease_providers_state(lease, enable: boolean) {
    val lease_providers = lease_dapp_provider @* { lease }.provider;
    if (exists(lease_providers)) {
        send_message(change_dapp_providers_state_topic, change_dapp_providers_state_message(
            providers = lease_providers,
            enable
        ).to_gtv());
    }
}

function delete_lease_dapp_providers(lease): list<pubkey> {
    val lease_providers = lease_dapp_provider @* { lease };
    val deleted_providers = list<pubkey>();

    for (lease_provider in lease_providers) {
        deleted_providers.add(lease_provider.provider);
        delete lease_provider;
    }

    return deleted_providers;
}

function transfer_dapp_providers_to_lease(lease, providers: list<pubkey>) {
    for (provider in providers) {
        create lease_dapp_provider(lease, provider);
    }
}

/**
 * Get provider account id.
 *
 * @param provider_pubkey  pubkey of provider.
 */
query get_provider_account_id(provider_pubkey: pubkey): byte_array? {
    val provider = require(provider @? { provider_pubkey }, "No provider found with pubkey %s".format(provider_pubkey));
    val account = provider_account @? { provider }.account;
    return account?.id;
}

query get_dapp_provider_creation_cost() = chain_context.args.dapp_provider_creation_cost;
