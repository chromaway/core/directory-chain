function expire_leases(current_time_millis: integer) {
    for (lease in lease @* { current_time_millis > .start_time + .duration_millis, .expired == false }) {
        // Don't touch leases for containers that are currently being upgraded
        if (not has_pending_upgrade(lease.container_name)) {
            maybe_expire_lease(lease);
        }
    }
}

function maybe_expire_lease(lease) {
    if (lease.auto_renew) {
        log("Auto-renewing lease for container %s".format(lease.container_name));
        val success = try_call(renew_container_lease(lease.account, lease, 1, *));
        if (not success) {
            log("Unable to auto-renew lease for container %s".format(lease.container_name));
            expire_lease(lease);
        }
    } else {
        expire_lease(lease);
    }
}

function expire_lease(lease) {
    lease.expired = true;
    log("Lease for container %s has expired, stopping it".format(lease.container_name));
    send_message(stop_container_topic, stop_container_message(container_name = lease.container_name).to_gtv()); // fire-and-forget
    on_lease_expired(lease);
}

function delete_expired_leases(current_time_millis: integer) {
    if (chain_context.args.container_removal_timeout < 0) return;

    for (lease in lease @* { current_time_millis > .start_time + .duration_millis + chain_context.args.container_removal_timeout, .expired }) {
        // Don't touch leases for containers that are currently being upgraded
        if (not has_pending_upgrade(lease.container_name)) {
            log("Lease for container %s has been expired longer than the allowed time, removing it".format(lease.container_name));
            send_message(remove_container_topic, remove_container_message(container_name = lease.container_name).to_gtv()); // fire-and-forget
            on_expired_lease_removed(lease);
            delete lease_subnode_image @? { lease };
            delete lease;
        }
    }
}

@extendable function on_lease_expired(lease) {}

@extendable function on_expired_lease_removed(lease) {}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(create_container).mount_name,
    flags = ["T"],
    message = create_container_auth_message(*)
);

function create_container_auth_message(gtv): text {
    val args = struct<create_container>.from_gtv(gtv);
    val tag = require_cluster(args.cluster_name).tag;
    val cost = calculate_container_cost(args.duration_weeks, args.container_units, args.extra_storage_gib, tag);
    return "Please sign the message\nto lease container with %s SCUs and %s GiB extra storage\nin cluster %s\nfor %s weeks\ncosting %s %s\n%son account {account_id}".format(
        args.container_units,
        args.extra_storage_gib,
        args.cluster_name,
        args.duration_weeks,
        ft4.assets.format_amount_with_decimals(cost, chain_context.args.asset_decimals),
        chain_context.args.asset_symbol,
        if (args.auto_renew) "with auto-renewal\n" else "");
}

/**
 * Request creation of a container with standard subnode image.
 *
 * If enough tokens are available on the users account an ICMF message will be sent to DC for creation of the container.
 * A lease is setup for the request amount of weeks and the cost is deducted from the users account.
 *
 * @param provider_pubkey  pubkey of provider
 * @param container_units  number of container_units
 */
operation create_container(provider_pubkey: pubkey, container_units: integer,
    duration_weeks: integer, extra_storage_gib: integer, cluster_name: text, auto_renew: boolean) {
    create_container_impl(provider_pubkey, container_units, duration_weeks, extra_storage_gib, cluster_name, auto_renew, "");
}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(create_container_with_subnode_image).mount_name,
    flags = ["T"],
    message = create_container_with_subnode_image_auth_message(*)
);

function create_container_with_subnode_image_auth_message(gtv): text {
    val args = struct<create_container_with_subnode_image>.from_gtv(gtv);
    val tag = require_cluster(args.cluster_name).tag;
    val cost = calculate_container_cost(args.duration_weeks, args.container_units, args.extra_storage_gib, tag);
    return "Please sign the message\nto lease container with %s SCUs and %s GiB extra storage%s\nin cluster %s\nfor %s weeks\ncosting %s %s\n%son account {account_id}".format(
        args.container_units,
        args.extra_storage_gib,
        if (args.subnode_image_name.empty()) "" else " and subnode image " + args.subnode_image_name,
        args.cluster_name,
        args.duration_weeks,
        ft4.assets.format_amount_with_decimals(cost, chain_context.args.asset_decimals),
        chain_context.args.asset_symbol,
        if (args.auto_renew) "with auto-renewal\n" else "");
}

/**
 * Request creation of a container with custom subnode image.
 *
 * If enough tokens are available on the users account an ICMF message will be sent to DC for creation of the container.
 * A lease is setup for the request amount of weeks and the cost is deducted from the users account.
 *
 * @param provider_pubkey  pubkey of provider
 * @param container_units  number of container_units
 * @param subnode_image_name  subnode image to use, or "" to use default subnode image
 */
operation create_container_with_subnode_image(provider_pubkey: pubkey, container_units: integer,
    duration_weeks: integer, extra_storage_gib: integer, cluster_name: text, auto_renew: boolean, subnode_image_name: text) {
    create_container_impl(provider_pubkey, container_units, duration_weeks, extra_storage_gib, cluster_name, auto_renew, subnode_image_name);
}

function create_container_impl(
    provider_pubkey: pubkey,
    container_units: integer,
    duration_weeks: integer,
    extra_storage_gib: integer,
    cluster_name: text,
    auto_renew: boolean,
    subnode_image_name: text) {

    require_initialized();
    require_pubkey(provider_pubkey);
    require(container_units > 0, "container_units must be positive");
    require_lease_duration(duration_weeks);
    require(extra_storage_gib > -1, "extra_storage_gib must not be negative");
    require_dapp_cluster(cluster_name);

    val cluster = require_cluster(cluster_name);

    if (not subnode_image_name.empty()) {
        val subnode_image = require(subnode_image @? { .name == subnode_image_name }, "Subnode image %s not found".format(subnode_image_name));
        require(subnode_image.active, "Subnode image %s is not active".format(subnode_image_name));
        require(subnode_image.subnode_image_type == subnode_image_type.COMMON or not empty(cluster_subnode_image @? { cluster, subnode_image }),
            "Subnode image %s is not compatible with cluster %s".format(subnode_image_name, cluster.name));
    }

    val account = ft4.auth.authenticate();
    require_non_memo_account(account);

    val cost = calculate_container_cost(duration_weeks, container_units, extra_storage_gib, cluster.tag);
    ft4.assets.Unsafe.transfer(account, get_pool_account(), get_asset(), cost);

    val ticket = create ticket(type = ticket_type.CREATE_CONTAINER, account);
    create create_container_ticket(ticket,
        container_units = container_units,
        extra_storage_gib = extra_storage_gib,
        duration_millis = duration_weeks * millis_per_week,
        cost = cost,
        auto_renew = auto_renew,
        cluster_name = cluster.name,
        subnode_image_name = subnode_image_name,
    );
    send_message(create_container_topic, create_container_message(
        ticket_id = ticket.rowid.to_integer(),
        provider_pubkey = provider_pubkey,
        container_units = container_units,
        extra_storage = 1024 * extra_storage_gib,
        cluster_name = cluster_name,
        subnode_image_name = subnode_image_name,
    ).to_gtv());

    create lease_purchase(account, duration_millis = duration_weeks * millis_per_week, container_units, extra_storage_gib, price = cost);
}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(assign_subnode_image_to_container).mount_name,
    flags = ["T"],
    message = assign_subnode_image_to_container_auth_message(*)
);

function assign_subnode_image_to_container_auth_message(gtv): text {
    val args = struct<assign_subnode_image_to_container>.from_gtv(gtv);
    return "Please sign the message\nto assign subnode image %s\nto container %s\non account {account_id}".format(
        args.subnode_image_name,
        args.container_name,
    );
}

/**
 * Request assign a custom subnode image to an existing container.
 *
 * An ICMF message will be sent to DC for updating of the container.
 *
 * @param container_name  container name
 * @param subnode_image_name  subnode image to use
 */
operation assign_subnode_image_to_container(container_name: text, subnode_image_name: text) {
    require_initialized();
    val lease = require_lease(container_name);

    require(not subnode_image_name.empty(), "Subnode image name cannot be empty");
    val subnode_image = require(subnode_image @? { .name == subnode_image_name }, "Subnode image %s not found".format(subnode_image_name));
    require(subnode_image.active, "Subnode image %s is not active".format(subnode_image_name));
    require(subnode_image.subnode_image_type == subnode_image_type.COMMON or not empty(cluster_subnode_image @? { lease.cluster, subnode_image }),
        "Subnode image %s is not compatible with cluster %s".format(subnode_image_name, lease.cluster.name));

    require(not has_pending_subnode_image_assignment(container_name), "Container %s already has a pending subnode image assignment".format(container_name));
    require(empty(lease_subnode_image @? { lease }), "Container %s already has a custom subnode image".format(container_name));

    val account = ft4.auth.authenticate();
    require(lease.account == account, "Lease for container %s was not created by you".format(container_name));

    val ticket = create ticket(type = ticket_type.ASSIGN_SUBNODE_IMAGE_TO_CONTAINER, account);
    create assign_subnode_image_to_container_ticket(ticket,
        container_name = container_name,
        subnode_image_name = subnode_image_name,
    );
    send_message(assign_subnode_image_to_container_topic, assign_subnode_image_to_container_message(
        ticket_id = ticket.rowid.to_integer(),
        container_name = container_name,
        subnode_image_name = subnode_image_name,
    ).to_gtv());
}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(upgrade_container).mount_name,
    flags = ["T"],
    message = upgrade_container_auth_message(*)
);

function upgrade_container_auth_message(gtv): text {
    val args = struct<upgrade_container>.from_gtv(gtv);
    require_lease(args.container_name);
    val tag = require_cluster(args.upgraded_cluster_name).tag;
    val cost = calculate_container_cost(args.upgraded_duration_weeks, args.upgraded_container_units, args.upgraded_extra_storage_gib, tag);
    return "Please sign the message\nto upgrade container %s\nto have %s SCUs and %s GiB extra storage\nin cluster %s\nfor %s weeks\ncosting (before applying refund of current lease) %s %s\non account {account_id}".format(
        args.container_name,
        args.upgraded_container_units,
        args.upgraded_extra_storage_gib,
        args.upgraded_cluster_name,
        args.upgraded_duration_weeks,
        ft4.assets.format_amount_with_decimals(cost, chain_context.args.asset_decimals),
        chain_context.args.asset_symbol
    );
}

/**
 * Offer container lease ownership transfer to a receiver FT4 account.
 * ft4.ft_auth operation by current container lease owner is required to offer lease ownership transfer.
 * Receiver FT4 account needs to accept the transfer by using the `operation accept_container_lease_ownership_transfer`
 * @param container_name container lease to be transferred
 * @param receiver_account_id FT4 account to receive lease ownership
 */
operation offer_container_lease_ownership_transfer(container_name: text, receiver_account_id: byte_array) {
    require_initialized();
    val lease = require_lease(container_name);
    val receiver_account = ft4.accounts.account_by_id(receiver_account_id);
    val account = ft4.auth.authenticate();
    require(lease.account == account, "Lease for container: %s was not created by you.".format(container_name));
    require(account != receiver_account, "You cannot transfer lease ownership to yourself.");
    require(not(exists(container_lease_ownership_transfer_offer @? { container_name })), "Offer for container: %s already exists!".format(container_name));
    create container_lease_ownership_transfer_offer(container_name, receiver_account);
}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(offer_container_lease_ownership_transfer).mount_name,
    flags = ["T"],
    message = offer_container_lease_ownership_transfer_auth_message(*)
);

function offer_container_lease_ownership_transfer_auth_message(gtv): text {
    val args = struct<offer_container_lease_ownership_transfer>.from_gtv(gtv);
    return "Please sign the message\nto offer transfer of lease ownership of container %s\nto account %s".format(
        args.container_name,
        args.receiver_account_id,
    );
}

/**
 * Remove offer lease ownership transfer.
 * ft4.ft_auth operation by current container lease owner is required to remove offer.
 * @param container_name container lease to be transferred
 */
operation remove_container_lease_ownership_transfer_offer(container_name: text) {
    require_initialized();
    val lease = require_lease(container_name);
    val account = ft4.auth.authenticate();
    require(lease.account == account, "Lease for container %s was not created by you.".format(container_name));
    delete container_lease_ownership_transfer_offer @* { container_name };
}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(remove_container_lease_ownership_transfer_offer).mount_name,
    flags = ["T"],
    message = remove_container_lease_ownership_transfer_offer_auth_message(*)
);

function remove_container_lease_ownership_transfer_offer_auth_message(gtv): text {
    val args = struct<remove_container_lease_ownership_transfer_offer>.from_gtv(gtv);
    val receiver_account_id = container_lease_ownership_transfer_offer @? { args.container_name } (.receiver_account.id);
    return "Please sign the message\nto remove lease ownership transfer offer of container %s\nto %s".format(
        args.container_name,
        receiver_account_id
    );
}

/**
 * Accept offered lease ownership transfer.
 * ft4.ft_auth operation by container lease receiver account is required to accept transfer offer.
 * @param container_name container lease to be transferred
 */
operation accept_container_lease_ownership_transfer_offer(container_name: text) {
    val lease = require_lease(container_name);
    val receiver_account = ft4.auth.authenticate();
    val transfer = container_lease_ownership_transfer_offer @? { container_name, receiver_account };
    require(transfer, "There is no lease ownership transfer offer for you to accept.");

    val current_lease = lease @ { transfer.container_name };
    current_lease.account = receiver_account;
    delete transfer;
}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(accept_container_lease_ownership_transfer_offer).mount_name,
    flags = ["T"],
    message = accept_container_lease_ownership_transfer_offer_auth_message(*)
);

function accept_container_lease_ownership_transfer_offer_auth_message(gtv): text {
    val args = struct<accept_container_lease_ownership_transfer_offer>.from_gtv(gtv);
    return "Please sign the message\nto accept transfer of lease ownership of container %s\n".format(
        args.container_name
    );
}

/**
 * If possible upgrade (or downgrade) the container to requested specifications.
 *
 * `warning:` Please note that reducing the storage only works as long as the container still has at least
 * 20% of free space (or what is configured in `container_reduce_space_margin_requirement`).
 *
 * @param upgraded_container_units The new number of container units for this container. Can both be increased and reduced.
 * @param upgraded_extra_storage_gib The new size of extra storage.
 * @param upgraded_cluster_name Move this container to a new cluster.
 * @param upgraded_duration_weeks Duration for the new upgraded lease
 */
operation upgrade_container(container_name: text, upgraded_container_units: integer,
    upgraded_extra_storage_gib: integer, upgraded_cluster_name: text, upgraded_duration_weeks: integer) {
    require_initialized();
    val lease = require_lease(container_name);
    require(not has_pending_upgrade(container_name), "Container %s already has a pending upgrade".format(container_name));
    require(
        upgraded_container_units != lease.container_units or upgraded_extra_storage_gib != lease.extra_storage_gib,
        "New container specifications are identical to current lease"
    );
    require_lease_duration(upgraded_duration_weeks);

    val current_total_storage_mib = calculate_lease_storage_mib(lease);
    val upgraded_total_storage_mib = calculate_storage_mib(upgraded_container_units, upgraded_extra_storage_gib);
    require_enough_space(container_name, current_total_storage_mib, upgraded_total_storage_mib);
    require_dapp_cluster(upgraded_cluster_name);

    val account = ft4.auth.authenticate();
    require(lease.account == account, "Lease for container %s was not created by you".format(container_name));
    require_non_memo_account(account);

    val cluster = require_cluster(upgraded_cluster_name);

    val cost = calculate_container_cost(upgraded_duration_weeks, upgraded_container_units, upgraded_extra_storage_gib, cluster.tag)
        - if (lease.expired) 0 else calculate_remaining_lease_value(lease, op_context.last_block_time);
    if (cost > 0) {
        ft4.assets.Unsafe.transfer(account, get_pool_account(), get_asset(), cost);
    } else if (cost < 0) {
        ft4.assets.Unsafe.transfer(get_pool_account(), account, get_asset(), abs(cost));
    }

    val ticket = create ticket(type = ticket_type.UPGRADE_CONTAINER, account);
    create upgrade_container_ticket(ticket,
        container_name = container_name,
        container_units = upgraded_container_units,
        extra_storage_gib = upgraded_extra_storage_gib,
        cost = cost,
        cluster_name = cluster.name,
        duration_weeks = upgraded_duration_weeks
    );
    send_message(upgrade_container_topic, upgrade_container_message(
        ticket_id = ticket.rowid.to_integer(),
        container_name = container_name,
        container_units = upgraded_container_units,
        extra_storage = 1024 * upgraded_extra_storage_gib,
        cluster_name = upgraded_cluster_name
    ).to_gtv());

    create lease_purchase(
        account, duration_millis = lease.duration_millis,
        container_units = upgraded_container_units, extra_storage_gib = upgraded_extra_storage_gib,
        price = cost, lease_purchase_type.UPGRADE
    );
}

function calculate_lease_storage_mib(lease) {
    return calculate_storage_mib(lease.container_units, lease.extra_storage_gib);
}

function calculate_storage_mib(container_units: integer, extra_storage_gib: integer) {
    return container_units * standard_cluster_unit.storage + extra_storage_gib * 1024;
}

function require_enough_space(container_name: text, current_total_storage_mib: integer, upgraded_total_storage_mib: integer) {
    if (upgraded_total_storage_mib < current_total_storage_mib) {
        val most_space_usage_mib = get_most_container_space_usage_mib(container_name);
        require(most_space_usage_mib != null, "Can't reduce space: No space data available for container %s"
            .format(container_name));
        require(is_anchoring_data_up_to_date(container_name),
            "Can't reduce space: Container space data is too old to be able to continue");

        val req_margin_in_mib = integer(most_space_usage_mib * chain_context.args.container_reduce_space_margin_requirement);
        val req_storage_in_mib = most_space_usage_mib + req_margin_in_mib;

        require(upgraded_total_storage_mib >= req_storage_in_mib,
            "Can't reduce space: %s is not enough container space. Container is currently using %d MiB, and with a margin of %d MiB the minimum total allowed space is %d MiB".format(
                upgraded_total_storage_mib, most_space_usage_mib, req_margin_in_mib, req_storage_in_mib));
    }
}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(renew_container).mount_name,
    flags = ["T"],
    message = renew_container_auth_message(*)
);

function renew_container_auth_message(gtv): text {
    val args = struct<renew_container>.from_gtv(gtv);

    val lease = require_lease(args.container_name);
    val tag = lease.cluster.tag;
    val cost = calculate_container_cost(args.duration_weeks, lease.container_units, lease.extra_storage_gib, tag);

    return "Please sign the message\nto renew lease of container %s\nfor %s weeks\ncosting %s %s\non account {account_id}".format(
        args.container_name,
        args.duration_weeks,
        ft4.assets.format_amount_with_decimals(cost, chain_context.args.asset_decimals),
        chain_context.args.asset_symbol);
}

/**
 * Renew container lease.
 *
 * @param container_name  container name
 * @param duration_weeks  lease duration in weeks
 */
operation renew_container(container_name: text, duration_weeks: integer) {
    require_initialized();
    require_lease_duration(duration_weeks);

    val account = ft4.auth.authenticate();

    val lease = require_lease(container_name);

    renew_container_lease(account, lease, duration_weeks);
}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(auto_renew_container).mount_name,
    flags = ["T"],
    message = auto_renew_container_auth_message(*)
);

function auto_renew_container_auth_message(gtv): text {
    val args = struct<auto_renew_container>.from_gtv(gtv);

    val lease = require_lease(args.container_name);
    val tag = lease.cluster.tag;
    val weekly_cost = calculate_container_cost(1, lease.container_units, lease.extra_storage_gib, tag);

    return "Please sign the message\nto enable auto-renewal of container %s\ncosting %s %s per week\non account {account_id}".format(
        args.container_name,
        ft4.assets.format_amount_with_decimals(weekly_cost, chain_context.args.asset_decimals),
        chain_context.args.asset_symbol);
}

/**
 * Turn on auto-renewal of lease for container.
 *
 * @param container_name  container name
 */
operation auto_renew_container(container_name: text) {
    require_initialized();

    val account = ft4.auth.authenticate();

    val lease = require_lease(container_name);
    require(lease.account == account, "Lease for container %s was not created by you".format(container_name));
    require(lease.auto_renew == false, "Container %s is already auto-renewing".format(container_name));

    lease.auto_renew = true;

    if (lease.expired) {
        renew_container_lease(account, lease, 1);
    }
}

@extend(ft4.auth.auth_handler)
function () = ft4.auth.add_auth_handler(
    scope = rell.meta(cancel_renew_container).mount_name,
    flags = ["T"],
    message = cancel_renew_container_auth_message(*)
);

function cancel_renew_container_auth_message(gtv): text {
    val args = struct<cancel_renew_container>.from_gtv(gtv);

    require_lease(args.container_name);

    return "Please sign the message\nto cancel auto-renewal of container %s\non account {account_id}".format(
        args.container_name);
}

/**
 * Cancel auto-renewal of lease.
 *
 * @param container_name  container name
 */
operation cancel_renew_container(container_name: text) {
    require_initialized();

    val account = ft4.auth.authenticate();

    val lease = require_lease(container_name);
    require(lease.account == account, "Lease for container %s was not created by you".format(container_name));
    require(lease.auto_renew == true, "Container %s is not auto-renewing".format(container_name));

    lease.auto_renew = false;
}

function renew_container_lease(ft4.accounts.account, lease, duration_weeks: integer) {
    require(not has_pending_upgrade(lease.container_name), "Can't renew lease of container with a pending upgrade");

    val tag = lease.cluster.tag;
    val cost = calculate_container_cost(duration_weeks, lease.container_units, lease.extra_storage_gib, tag);
    ft4.assets.Unsafe.transfer(account, get_pool_account(), get_asset(), cost);
    create lease_purchase(
        account, duration_millis = duration_weeks * millis_per_week,
        container_units = lease.container_units, extra_storage_gib = lease.extra_storage_gib,
        price = cost, lease_purchase_type.RENEWAL
    );

    if (lease.expired) {
        log("Lease for container %s was expired, restarting it".format(lease.container_name));
        restart_container(lease.container_name);

        lease.expired = false;
        lease.duration_millis = (op_context.last_block_time - lease.start_time) + duration_weeks * millis_per_week;
        on_lease_resurrected(lease);
    } else {
        lease.duration_millis += duration_weeks * millis_per_week;
    }
}

function restart_container(container_name: text) {
    // TODO what if restart fails?
    send_message(restart_container_topic, restart_container_message(container_name).to_gtv()); // fire-and-forget
}

@extendable function on_lease_resurrected(lease) {}

function calculate_container_cost(duration_weeks: integer, container_units: integer, extra_storage_gib: integer, tag: tag): integer {
    val scu_cost = container_units * tag.scu_price;
    val extra_storage_cost = extra_storage_gib * tag.extra_storage_price;

    return (decimal(scu_cost + extra_storage_cost) / units_per_usd * 7 * duration_weeks * economy_constants.chr_per_usd * units_per_asset).to_integer();
}

function calculate_remaining_lease_value(lease, current_time: integer): integer {
    val tag = lease.cluster.tag;
    val lease_weekly_cost = calculate_container_cost(1, lease.container_units, lease.extra_storage_gib, tag);
    val remaining_lease_time = lease.start_time + lease.duration_millis - current_time;
    val remaining_weeks = decimal(remaining_lease_time) / decimal(millis_per_week);
    val remaining_value = remaining_weeks * lease_weekly_cost;
    return remaining_value.round().to_integer();
}

function refund_lease(lease: lease) {

    if (not lease.expired) {
        val lease_value = calculate_remaining_lease_value(lease, op_context.last_block_time);
        if (not try_call(ft4.assets.Unsafe.transfer(get_pool_account(), lease.account, get_asset(), lease_value, *))) {
            log("Unable to refund lease with container name %s".format(lease.container_name));
        } else {
            create lease_refund(lease.account, lease_value);
        }
    }

}

/**
 * Returns information for any ticket by transaction RID.
 *
 * @param tx_rid  transaction RID
 */
query get_ticket_by_transaction(tx_rid: byte_array): ticket_data? =
        ticket @? { .transaction.tx_rid == tx_rid }
            (ticket_data(.rowid.to_integer(), .type, .state, .error_message));

/**
 * Returns information for any ticket by ID.
 *
 * @param ticket_id  ticket ID
 */
query get_ticket_by_id(ticket_id: integer): ticket_data? =
        ticket @? { .rowid == rowid(ticket_id) }
            (ticket_data(.rowid.to_integer(), .type, .state, .error_message));

/**
 * Returns ticket information for the create container ticket by transaction RID.
 *
 * @param tx_rid  transaction RID
 */
query get_create_container_ticket_by_transaction(tx_rid: byte_array): container_ticket_data? =
        create_container_ticket @? { .ticket.transaction.tx_rid == tx_rid }
            (container_ticket_data(.ticket.rowid.to_integer(), .ticket.type, .ticket.state, error_message = .ticket.error_message, container_name = .container_name));

/**
 * Returns ticket information for the create container ticket by ticket ID.
 *
 * @param ticket_id  ticket ID
 */
query get_create_container_ticket_by_id(ticket_id: integer): container_ticket_data? =
        create_container_ticket @? { .ticket.rowid == rowid(ticket_id) }
            (container_ticket_data(.ticket.rowid.to_integer(), .ticket.type, .ticket.state, error_message = .ticket.error_message, container_name = .container_name));

/**
 * Returns ticket information for the upgrade container ticket by transaction RID.
 *
 * @param tx_rid  transaction RID
 */
query get_upgrade_container_ticket_by_transaction(tx_rid: byte_array): container_ticket_data? =
        upgrade_container_ticket @? { .ticket.transaction.tx_rid == tx_rid }
            (container_ticket_data(.ticket.rowid.to_integer(), .ticket.type, .ticket.state, error_message = .ticket.error_message, container_name = .container_name));

/**
 * Returns ticket information for the upgrade container ticket by ticket ID.
 *
 * @param ticket_id  ticket ID
 */
query get_upgrade_container_ticket_by_id(ticket_id: integer): container_ticket_data? =
        upgrade_container_ticket @? { .ticket.rowid == rowid(ticket_id) }
            (container_ticket_data(.ticket.rowid.to_integer(), .ticket.type, .ticket.state, error_message = .ticket.error_message, container_name = .container_name));

/**
 * Returns ticket information for assigning subnode image to container ticket by transaction RID.
 *
 * @param tx_rid  transaction RID
 */
query get_assign_subnode_image_to_container_ticket_by_transaction(tx_rid: byte_array): container_ticket_data? =
        assign_subnode_image_to_container_ticket @? { .ticket.transaction.tx_rid == tx_rid }
            (container_ticket_data(.ticket.rowid.to_integer(), .ticket.type, .ticket.state, error_message = .ticket.error_message, container_name = .container_name));

/**
 * Returns ticket information for assigning subnode image to container ticket by ticket ID.
 *
 * @param ticket_id  ticket ID
 */
query get_assign_subnode_image_to_container_ticket_by_id(ticket_id: integer): container_ticket_data? =
        assign_subnode_image_to_container_ticket @? { .ticket.rowid == rowid(ticket_id) }
            (container_ticket_data(.ticket.rowid.to_integer(), .ticket.type, .ticket.state, error_message = .ticket.error_message, container_name = .container_name));

/**
 * Get leases for an account.
 */
query get_leases_by_account(account_id: byte_array): list<lease_data> =
        (lease, @outer lease_subnode_image @* { .lease == lease }) @* { .account.id == account_id }
            (lease_data(
                container_name = .container_name,
                cluster_name = .cluster.name,
                container_units = .container_units,
                extra_storage_gib = .extra_storage_gib,
                expire_time_millis = .start_time + .duration_millis,
                .expired,
                .auto_renew,
                subnode_image_name = .subnode_image?.name ?: "",
                bridge_leases = get_bridge_leases_for_lease(lease)
        ));

/**
 * Get current lease for a container.
 *
 * @param container_name  container name
 */
query get_lease_by_container_name(container_name: text): lease_data? =
        (lease, @outer lease_subnode_image @* { .lease == lease }) @? { container_name }
            (lease_data(
                container_name = .container_name,
                cluster_name = .cluster.name,
                container_units = .container_units,
                extra_storage_gib = .extra_storage_gib,
                expire_time_millis = .start_time + .duration_millis,
                .expired,
                .auto_renew,
                 subnode_image_name = .subnode_image?.name ?: "",
                bridge_leases = get_bridge_leases_for_lease(lease)
        ));

/**
 * Get minimum lease time in weeks.
 */
query get_min_lease_duration(): integer = economy_constants.min_lease_time_weeks;

/**
 * Get maximum lease time in weeks.
 */
query get_max_lease_duration(): integer = economy_constants.max_lease_time_weeks;

query get_lease_purchases(page_size: integer?, page_cursor: text?): ft4.utils.paged_result {
    val before_rowid = ft4.utils.before_rowid(page_cursor);

    val paginated_result = lease_purchase @* {
        .rowid > (before_rowid ?: rowid(0))
    } (
        ft4.utils.pagination_result(
            data = lease_purchase_data(
                account_id = .account.id,
                duration_millis = .duration_millis,
                container_units = .container_units,
                extra_storage_gib = .extra_storage_gib,
                price = .price,
                type = .type,
                timestamp = .transaction.block.timestamp,
                tx_rid = .transaction.tx_rid
            ).to_gtv_pretty(),
            rowid = .rowid
        )
    ) limit ft4.utils.fetch_data_size(page_size);

    return ft4.utils.make_page(paginated_result, page_size);
}

query get_lease_refunds(page_size: integer?, page_cursor: text?): ft4.utils.paged_result {
    val before_rowid = ft4.utils.before_rowid(page_cursor);

    val paginated_result = lease_refund @* {
        .rowid > (before_rowid ?: rowid(0))
    } (
        ft4.utils.pagination_result(
            data = lease_refund_data(
                .account.id,
                .amount
            ).to_gtv_pretty(),
            rowid = .rowid
        )
    ) limit ft4.utils.fetch_data_size(page_size);

    return ft4.utils.make_page(paginated_result, page_size);
}

query get_lease_purchases_by_account(account_id: byte_array, page_size: integer?, page_cursor: text?): ft4.utils.paged_result {
    val before_rowid = ft4.utils.before_rowid(page_cursor);

    val paginated_result = lease_purchase @* {
        .account.id == account_id,
        .rowid > (before_rowid ?: rowid(0))
    } (
        ft4.utils.pagination_result(
            data = lease_purchase_data(
                account_id = account_id,
                duration_millis = .duration_millis,
                container_units = .container_units,
                extra_storage_gib = .extra_storage_gib,
                price = .price,
                type = .type,
                timestamp = .transaction.block.timestamp,
                tx_rid = .transaction.tx_rid
            ).to_gtv_pretty(),
            rowid = .rowid
        )
    ) limit ft4.utils.fetch_data_size(page_size);

    return ft4.utils.make_page(paginated_result, page_size);
}

query get_lease_refunds_by_account(account_id: byte_array, page_size: integer?, page_cursor: text?): ft4.utils.paged_result {
    val before_rowid = ft4.utils.before_rowid(page_cursor);

    val paginated_result = lease_refund @* {
        .account.id == account_id,
        .rowid > (before_rowid ?: rowid(0))
    } (
        ft4.utils.pagination_result(
            data = lease_refund_data(
                account_id,
                .amount
            ).to_gtv_pretty(),
            rowid = .rowid
        )
    ) limit ft4.utils.fetch_data_size(page_size);

    return ft4.utils.make_page(paginated_result, page_size);
}

query total_lease_investment(): integer {
    return lease_purchase @ {} (@sum .price) - lease_refund @ {} (@sum .amount);
}

query get_container_lease_ownership_transfer_offer(container_name: text): container_lease_ownership_transfer_offer_data? {
    return container_lease_ownership_transfer_offer @? { container_name } (container_lease_ownership_transfer_offer_data(.container_name, .receiver_account.id));
}
