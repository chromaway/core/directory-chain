module;

import messaging.economy_chain.*;
import messaging.bridge_mapping.*;
import messaging.node_availability_reporting.*;
import messaging.blockchain_rid.*;
import messaging.economy_chain_staking.*;
import messaging.resource_usage_statistics.*;
import lib.transaction_submitter_messaging.*;
import lib.icmf.*;
import lib.icmf.receiver.*;
import lib.price_oracle_messages.*;
import common_proposal.*;
import common_proposal.voter_set_proposal.*;
import common_vault_dapp_details.*;
import common_helpers.constants.*;
import common_helpers.proposals.*;
import common_helpers.resource_usage_statistics.*;
import common_helpers.block.*;
import constants.*;
import provider_auth.*;
import chain_version;
import delay.*;

namespace eif {
    import messaging: lib.eif.messaging;
    import lib.hbridge;
}

namespace ft4 {
    import lib.ft4.accounts;
    import lib.ft4.assets;
    import lib.ft4.auth;
    import lib.ft4.version;
    import lib.ft4.utils;
    import lib.ft4.accounts.strategies;
    import lib.ft4.accounts.strategies.transfer.fee;
    import lib.ft4.accounts.strategies.transfer.open;
    import lib.ft4.crosschain;
}

struct module_args {
    asset_name: text;
    asset_symbol: text;
    asset_icon: text;
    asset_decimals: integer;
    pool_amount_to_mint: big_integer;
    evm_bridges: gtv;
    staking_withdrawal_delay_ms: integer;
    staking_delegation_delay_ms: integer;
    staking_rewards_payout_interval_ms: integer;
    staking_rewards_payout_batch_size: integer;
    staking_withdrawals_payout_interval_ms: integer;
    staking_rewards_share: decimal;
    staking_rewards_start_time: timestamp = 0;
    pool_refill_limit_ms: integer;
    max_bridge_leases_per_container: integer;
    evm_transaction_submitters_bonus: decimal;
    max_dapp_providers_per_lease: integer = 100;
    dapp_provider_creation_cost: integer = 10000000; // 10 CHR
    staking_oracle_pubkey: pubkey;
    container_removal_timeout: integer;
    container_reduce_space_margin_requirement: decimal = 0.2; // The required margin of free space in percent to allow reducing space
    container_reduce_space_resource_usage_statistics_expire_ms: integer = 1000 * 60 * 10; // A resource usage data from anchoring chain must be sent within this time period to allow reducing space
    migration_staking_account_state_send_limit: integer = 100;
    token_rate_update_interval_ms: integer;
}

val units_per_asset = (10).pow(chain_context.args.asset_decimals);
val units_per_usd = (10).pow(chain_context.args.asset_decimals);
val units_per_txs_evm_cost = 10L.pow(18);

operation init() {
    require(state.initialized == false, "Economy chain is already initialized");
    state.initialized = true;

    val asset = register_assets();
    register_and_link_bridge_and_erc20_asset(asset);

    create_pool_account();

    create_deposit_account();

    create_chromia_foundation_account();

    __staking_init();
    set_initial_token_rates();
    set_initial_config_hash(chain_context.raw_config.hash());

    log("Economy chain was initialized");
}

operation __begin_block(height: integer) {

    do_migrations();

    if (state.initialized) {
        if (not try_call(handle_configuration_update(*))) log("Handling configuration update failed");
        if (not try_call(expire_leases(op_context.last_block_time, *))) log("Failed lease expiry.");
        if (not try_call(delete_expired_leases(op_context.last_block_time, *))) log("Failed to delete expired leases.");
        if (not try_call(__staking_begin_block(height, *))) log("Failed staking begin block hook.");
        if (not try_call(__evm_links_begin_block(*))) log("Failed evm links begin block hook.");
        if (not try_call(price_oracle_updates_begin_block(op_context.last_block_time, *))) log("Failed price oracle begin block hook.");
        if (not try_call(begin_block_delay(height, *))) log("Failed to run delayed jobs");
    }
}

function _fn_evm_block(network_id: integer, evm_block_height: integer, evm_block_hash: byte_array, events: list<eif.messaging.event_data>) {
    __handle_staking_events(network_id, events);
}

function require_initialized() {
    require(state.initialized == true, "Economy chain is not initialized");
}

function require_pubkey(pubkey) {
    require(
            pubkey.size() == 33 // ECDSA
            or pubkey.size() == 1336, // Dilithium
        "Wrong size of public key: %d".format(pubkey.size()));
}

@extend(eif.messaging.handle_evm_block)
function (evm_block: eif.messaging.evm_block_message) {
    __handle_staking_events(evm_block.network_id, evm_block.events);
}

@extend(vault_dapp_details) function ec_vault_dapp_details() = vault_dapp_details_data(
    name = "Chromia Economy Chain",
    description = "Economy chain handles native CHR payments, container leases and provider rewards",
    icon_media_url = "https://assets.chromia.com/chr.png",
    other_media_url = "https://assets.chromia.com/chr-1000.png",
);
