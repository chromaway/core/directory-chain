@test module;

import ^^.*;
import ^.helper_functions.*;

function test_propose_staking_requirement_constants_require_system_provider_signer() {

    rell.test.tx().op(
        propose_staking_requirement_constants(null, null, null, null, null, null)
    ).run_must_fail("Expected system provider signature");
}

function test_propose_staking_requirement_constants_validate_input() {

    add_provider(rell.test.keypairs.alice.pub, true, provider_tier.NODE_PROVIDER);

    rell.test.tx().op(
        propose_staking_requirement_constants(null, -2, null, null, null, null)
    ).sign(rell.test.keypairs.alice).run_must_fail("Stop payout days must be greater or equal to 0");

    rell.test.tx().op(
        propose_staking_requirement_constants(null, null, -2, null, null, null)
    ).sign(rell.test.keypairs.alice).run_must_fail("System node own stake chr must be greater or equal to 0");

    rell.test.tx().op(
        propose_staking_requirement_constants(null, null, null, -2, null, null)
    ).sign(rell.test.keypairs.alice).run_must_fail("System node total stake chr must be greater or equal to 0");

    rell.test.tx().op(
        propose_staking_requirement_constants(null, null, null, null, -2, null)
    ).sign(rell.test.keypairs.alice).run_must_fail("Dapp node own stake chr must be greater or equal to 0");

    rell.test.tx().op(
        propose_staking_requirement_constants(null, null, null, null, null, -2)
    ).sign(rell.test.keypairs.alice).run_must_fail("Dapp node total stake chr must be greater or equal to 0");
}

function test_propose_staking_requirement_constants_all() {

    // Make sure values are not what we will set them to
    val constants: economy_constants_data = get_economy_constants();
    assert_not_equals(constants.staking_requirements_enabled, true);
    assert_not_equals(constants.staking_requirement_stop_payout_days, 1);
    assert_not_equals(constants.staking_requirement_system_node_own_stake_chr, 2);
    assert_not_equals(constants.staking_requirement_system_node_total_stake_chr, 3);
    assert_not_equals(constants.staking_requirement_dapp_node_own_stake_chr, 4);
    assert_not_equals(constants.staking_requirement_dapp_node_total_stake_chr, 5);

    add_provider(rell.test.keypairs.alice.pub, true, provider_tier.NODE_PROVIDER);
    rell.test.tx().op(
        propose_staking_requirement_constants(true, 1, 2, 3, 4, 5)
    ).sign(rell.test.keypairs.alice).run();

    val updated_constants: economy_constants_data = get_economy_constants();
    assert_equals(updated_constants.staking_requirements_enabled, true);
    assert_equals(updated_constants.staking_requirement_stop_payout_days, 1);
    assert_equals(updated_constants.staking_requirement_system_node_own_stake_chr, 2);
    assert_equals(updated_constants.staking_requirement_system_node_total_stake_chr, 3);
    assert_equals(updated_constants.staking_requirement_dapp_node_own_stake_chr, 4);
    assert_equals(updated_constants.staking_requirement_dapp_node_total_stake_chr, 5);

    assert_true(empty(pending_staking_requirement_constants @* { }));
}

function test_propose_staking_requirement_constants_partial() {

    var constants: economy_constants_data = get_economy_constants();
    assert_equals(constants.staking_requirements_enabled, false);
    assert_equals(constants.staking_requirement_stop_payout_days, 14);
    assert_equals(constants.staking_requirement_system_node_own_stake_chr, 60000 * units_per_asset);
    assert_equals(constants.staking_requirement_system_node_total_stake_chr, 600000 * units_per_asset);
    assert_equals(constants.staking_requirement_dapp_node_own_stake_chr, 30000 * units_per_asset);
    assert_equals(constants.staking_requirement_dapp_node_total_stake_chr, 300000 * units_per_asset);

    add_provider(rell.test.keypairs.alice.pub, true, provider_tier.NODE_PROVIDER);

    rell.test.tx().op(
        propose_staking_requirement_constants(true, 1, 2, null, null, null)
    ).sign(rell.test.keypairs.alice).run();

    constants = get_economy_constants();
    assert_equals(constants.staking_requirements_enabled, true);
    assert_equals(constants.staking_requirement_stop_payout_days, 1);
    assert_equals(constants.staking_requirement_system_node_own_stake_chr, 2);
    assert_equals(constants.staking_requirement_system_node_total_stake_chr, 600000 * units_per_asset);
    assert_equals(constants.staking_requirement_dapp_node_own_stake_chr, 30000 * units_per_asset);
    assert_equals(constants.staking_requirement_dapp_node_total_stake_chr, 300000 * units_per_asset);

    rell.test.tx().op(
        propose_staking_requirement_constants(null, null, null, 3, 4, 5)
    ).sign(rell.test.keypairs.alice).run();

    constants = get_economy_constants();
    assert_equals(constants.staking_requirements_enabled, true);
    assert_equals(constants.staking_requirement_stop_payout_days, 1);
    assert_equals(constants.staking_requirement_system_node_own_stake_chr, 2);
    assert_equals(constants.staking_requirement_system_node_total_stake_chr, 3);
    assert_equals(constants.staking_requirement_dapp_node_own_stake_chr, 4);
    assert_equals(constants.staking_requirement_dapp_node_total_stake_chr, 5);
}

function test_propose_staking_requirement_constants_scheduld_at() {
    add_provider(rell.test.keypairs.alice.pub, true, provider_tier.NODE_PROVIDER);
    add_provider(rell.test.keypairs.bob.pub, true, provider_tier.NODE_PROVIDER);
    val original_staking_requirement_stop_payout_days = get_economy_constants().staking_requirement_stop_payout_days;

    // Update constants
    rell.test.set_next_block_time_delta(1000);
    rell.test.tx().op(
        init(),
        propose_staking_requirement_constants(true, original_staking_requirement_stop_payout_days + 1, null, null, null, null, get_last_blocktime() + 5000)
    ).sign(rell.test.keypairs.alice).run();

    // Pending
    var proposal = get_common_proposal(get_latest_common_proposal_id())!!;
    assert_equals(proposal.state, common_proposal_state.PENDING);
    assert_null(proposal.apply_at);
    assert_not_null(proposal.scheduled_at);
    assert_equals(proposal.scheduled_at, get_last_blocktime() + 5000 - 1000);

    // Bob votes yes
    rell.test.set_next_block_time_delta(1000);
    vote_yes_on_latest_proposal(rell.test.keypairs.bob);

    // Delayed
    proposal = get_common_proposal(get_latest_common_proposal_id())!!;
    assert_equals(proposal.state, common_proposal_state.DELAYED);
    assert_equals(proposal.apply_at, get_last_blocktime() + 5000 - 2000);
    assert_not_null(proposal.scheduled_at);
    assert_equals(proposal.scheduled_at, get_last_blocktime() + 5000 - 2000);
    assert_equals(get_economy_constants().staking_requirement_stop_payout_days, original_staking_requirement_stop_payout_days);

    rell.test.set_next_block_time_delta(5000);
    rell.test.block().run();
    rell.test.block().run();

    // Applied
    proposal = get_common_proposal(get_latest_common_proposal_id())!!;
    assert_equals(proposal.state, common_proposal_state.APPROVED);
    assert_equals(get_economy_constants().staking_requirement_stop_payout_days, original_staking_requirement_stop_payout_days + 1);
    assert_true(empty(scheduled_event @* {}));
}

function test_get_staking_requirement_constants_proposal() {

    add_provider(rell.test.keypairs.alice.pub, true, provider_tier.NODE_PROVIDER);
    add_provider(rell.test.keypairs.bob.pub, true, provider_tier.NODE_PROVIDER);

    rell.test.tx().op(
        propose_staking_requirement_constants(false, 1, 2, 3, 4, 5)
    ).sign(rell.test.keypairs.alice).run();

    val proposal = get_staking_requirement_constants_proposal(get_latest_common_proposal_id());
    assert_equals(proposal.enabled, false);
    assert_equals(proposal.stop_payout_days, 1);
    assert_equals(proposal.system_node_own_stake_chr, 2);
    assert_equals(proposal.system_node_total_stake_chr, 3);
    assert_equals(proposal.dapp_node_own_stake_chr, 4);
    assert_equals(proposal.dapp_node_total_stake_chr, 5);
}
