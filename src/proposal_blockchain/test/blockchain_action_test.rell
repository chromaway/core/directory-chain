@test module;

import ^^.*;
import common.test.ops.*;
import common.test.setup.*;
import common.test.util.*;
import proposal.voting.test.ops.*;
import proposal.voting.test.helper_operations.*;
import proposal_container.*;
import direct_container.*;
import delay.test.helper_operations.*;
import ^.utils.*;

function setup() {
    setup_module();
    rell.test.tx().op(create_test_provider("bob", rell.test.pubkeys.bob)).run();
}

function test_propose_blockchain_action() {
    setup();

    val container = "container";
    val alice = provider @ { rell.test.pubkeys.alice };
    val bob = provider @ { rell.test.pubkeys.bob };
    rell.test.tx().op(add_test_member(system_p_voter_set(), bob)).run();

    var conf_foo = apply_required_configuration_properties(["name" : "foo".to_gtv()]);
    var conf_bar = apply_required_configuration_properties(["name" : "bar".to_gtv()]);
    rell.test.tx().op(
        create_container(alice.pubkey, container, clusters.system, 0, [alice.pubkey, bob.pubkey]),
        propose_blockchain(alice.pubkey, conf_foo.to_gtv().to_bytes(), "foo_chain", container),
        propose_blockchain(alice.pubkey, conf_bar.to_gtv().to_bytes(), "bar_chain", container)
    ).sign(rell.test.keypairs.alice).run();
    bob_votes();
    bob_votes();
    val foo_chain = blockchain @ { "foo_chain" };
    val bar_chain = blockchain @ { "bar_chain" };

    // Asserting blockchain is running
    assert_equals(foo_chain.state, blockchain_state.RUNNING);
    // Can't unarchive RUNNING blockchain
    bob_proposes_failing_action(foo_chain, blockchain_action.unarchive, "Running blockchain can't be unarchived");

    // 2. Proposing blockchain pause
    alice_proposes_action(foo_chain, blockchain_action.pause);
    assert_action_proposed(blockchain_action.pause);
    // Trying to propose
    bob_proposes_failing_action(foo_chain, blockchain_action.pause, "Blockchain action already proposed");
    // Continuing with the initial proposal
    bob_votes();
    assert_equals(foo_chain.state, blockchain_state.PAUSED);
    // Can't unarchive PAUSED blockchain
    bob_proposes_failing_action(foo_chain, blockchain_action.unarchive, "Paused blockchain can't be unarchived");

    // 3. Proposing blockchain resume
    alice_proposes_action(foo_chain, blockchain_action.resume);
    assert_action_proposed(blockchain_action.resume);
    bob_votes();
    assert_equals(foo_chain.state, blockchain_state.RUNNING);

    // 4. Proposing blockchain remove
    val brid = foo_chain.rid;
    alice_proposes_action(foo_chain, blockchain_action.remove);
    assert_action_proposed(blockchain_action.remove);
    bob_votes();
    assert_not_null( blockchain @? { .rid == brid } );
    assert_equals(foo_chain.state, blockchain_state.REMOVED);
    assert_false(empty(blockchain_configuration @* { foo_chain }));
    assert_false(empty(blockchain_configuration_signers @* { foo_chain }));

    // 5. Can't pause/resume/archive/unarchive/remove already removed blockchain
    bob_proposes_failing_action(foo_chain, blockchain_action.pause, "Removed blockchain can't be paused");
    bob_proposes_failing_action(foo_chain, blockchain_action.resume, "Removed blockchain can't be resumed");
    bob_proposes_failing_action(foo_chain, blockchain_action.archive, "Removed blockchain can't be archived");
    bob_proposes_failing_action(foo_chain, blockchain_action.unarchive, "Removed blockchain can't be unarchived");
    bob_proposes_failing_action(foo_chain, blockchain_action.remove, "Removed blockchain can't be removed");

    // 6. Can't pause/resume/archive already archived blockchain
    alice_proposes_action(bar_chain, blockchain_action.archive);
    bob_votes();
    assert_equals(bar_chain.state, blockchain_state.ARCHIVED);
    bob_proposes_failing_action(bar_chain, blockchain_action.pause, "Archived blockchain can't be paused");
    bob_proposes_failing_action(bar_chain, blockchain_action.resume, "Archived blockchain can't be resumed");
    bob_proposes_failing_action(bar_chain, blockchain_action.archive, "Blockchain is already archived");

    // 7. Proposing archived blockchain remove
    alice_proposes_action(bar_chain, blockchain_action.remove);
    bob_votes();
    assert_not_null( blockchain @? { bar_chain.rid } );
    assert_equals(bar_chain.state, blockchain_state.REMOVED);
    assert_false(empty(blockchain_configuration @* { bar_chain }));
    assert_false(empty(blockchain_configuration_signers @* { bar_chain }));

    // 8. See blockchain_action against MOVING blockchain state in proposal_blockchain_move.test.blockchain_move_test.rell
    // 9. See blockchain_action against UNARCHIVING blockchain state in .blockchain_archive_unarchive_action_test.rell
}

function test_config_proposals_delay_is_not_counted_while_paused() {
    setup();
    val container = "container";
    val alice = provider @ { rell.test.pubkeys.alice };
    val bob = provider @ { rell.test.pubkeys.bob };
    rell.test.tx().op(add_test_member(system_p_voter_set(), bob)).run();
    val conf = apply_required_configuration_properties(["name" : "conf".to_gtv()]);

    rell.test.set_next_block_time_delta(1);
    rell.test.tx().op(
        create_container(alice.pubkey, container, clusters.system, 0, [alice.pubkey, bob.pubkey]),
        propose_blockchain(alice.pubkey, conf.to_gtv().to_bytes(), "chain", container),
    ).sign(rell.test.keypairs.alice).run();
    bob_votes();
    val chain = blockchain @ { "chain" };

    // Pause the chain (and also any future config proposal)
    rell.test.set_block_interval(1);
    alice_proposes_action(chain, blockchain_action.pause);
    bob_votes();

    // Set a blockchain delay and create a config proposal
    rell.test.tx().op(
        set_blockchain_config_delay_op(chain, 100),
        propose_configuration(initial_provider.pub, chain.rid, conf.to_gtv().to_bytes())
    ).sign(rell.test.keypairs.alice).run();

    // Bob approves config proposal, it will be applied in 100ms
    bob_votes();

    // Delayed event still paused
    assert_true(exists(delayed_event @? { .active == false, .time == 100 }));

    // Unpause chain (and config proposal delay)
    alice_proposes_action(chain, blockchain_action.resume);
    rell.test.set_next_block_time_delta(100);
    bob_votes();

    // Delayed event active again
    assert_true(exists(delayed_event @? { .active == true, .time != 100 }));

    // Delayed event executed after 100 ms
    rell.test.block().run();
    assert_false(exists(delayed_event @? { }));
}

function test_config_proposals_scheduled_at_delay_is_not_counted_while_paused() {
    setup();
    val container = "container";
    val alice = provider @ { rell.test.pubkeys.alice };
    val bob = provider @ { rell.test.pubkeys.bob };
    rell.test.tx().op(add_test_member(system_p_voter_set(), bob)).run();
    val conf = apply_required_configuration_properties(["name" : "conf".to_gtv()]);

    rell.test.set_next_block_time_delta(1);
    rell.test.tx().op(
        create_container(alice.pubkey, container, clusters.system, 0, [alice.pubkey, bob.pubkey]),
        propose_blockchain(alice.pubkey, conf.to_gtv().to_bytes(), "chain", container),
    ).sign(rell.test.keypairs.alice).run();
    bob_votes();
    val chain = blockchain @ { "chain" };

    // Pause the chain (and also any future config proposal)
    rell.test.set_block_interval(1);
    alice_proposes_action(chain, blockchain_action.pause);
    bob_votes();

    // Set a blockchain delay and create a config proposal
    rell.test.tx().op(
        propose_configuration(initial_provider.pub, chain.rid, conf.to_gtv().to_bytes(), "", get_last_blocktime() + 100)
    ).sign(rell.test.keypairs.alice).run();

    // Bob approves config proposal, it will be applied in 100ms
    bob_votes();

    // Delayed event still paused
    assert_true(exists(delayed_event @? { .active == false, .time == 99 }));

    // Unpause chain (and config proposal delay)
    alice_proposes_action(chain, blockchain_action.resume);
    rell.test.set_next_block_time_delta(100);
    bob_votes();

    // Delayed event active again
    assert_true(exists(delayed_event @? { .active == true, .time != 100 }));

    // Delayed event executed after 100 ms
    rell.test.block().run();
    assert_false(exists(delayed_event @? { }));
}

function test_delayed_actions() {
    setup();

    val container = "container";
    val alice = provider @ { rell.test.pubkeys.alice };

    // 1 day delay
    var conf = apply_required_configuration_properties([DIRECTORY_CHAIN_CONFIG_KEY : [DIRECTORY_CHAIN_CONFIG_DELAY_CONFIG_KEY : 86400000].to_gtv()]);
    rell.test.tx().op(
        create_container(alice.pubkey, container, clusters.system, 0, [alice.pubkey]),
        propose_blockchain(alice.pubkey, conf.to_gtv().to_bytes(), "chain", container),
    ).sign(rell.test.keypairs.alice).run();
    val chain = blockchain @ { "chain" };

    // Verify pause is delayed
    alice_proposes_action(chain, blockchain_action.pause);

    assert_equals(chain.state, blockchain_state.RUNNING);
    assert_true(exists(delayed_event @? { .active }));

    rell.test.set_next_block_time_delta(86400000);
    rell.test.block().run();
    rell.test.block().run();

    assert_equals(chain.state, blockchain_state.PAUSED);
    assert_false(exists(delayed_event @? { .active }));

    alice_proposes_action(chain, blockchain_action.resume);

    // Verify removal is delayed
    alice_proposes_action(chain, blockchain_action.remove);

    assert_equals(chain.state, blockchain_state.RUNNING);
    assert_true(exists(delayed_event @? { .active }));

    rell.test.set_next_block_time_delta(86400000);
    rell.test.block().run();
    rell.test.block().run();

    assert_equals(chain.state, blockchain_state.REMOVED);
    assert_false(exists(delayed_event @? { .active }));
}

function test_future_delayed_actions_are_applied_immediately_if_delay_is_removed() {
    setup();

    val container = "container";
    val alice = provider @ { rell.test.pubkeys.alice };

    // 1 day delay
    val conf = apply_required_configuration_properties([DIRECTORY_CHAIN_CONFIG_KEY : [DIRECTORY_CHAIN_CONFIG_DELAY_CONFIG_KEY : 86400000].to_gtv()]);
    rell.test.tx().op(
        create_container(alice.pubkey, container, clusters.system, 0, [alice.pubkey]),
        propose_blockchain(alice.pubkey, conf.to_gtv().to_bytes(), "chain", container),
    ).sign(rell.test.keypairs.alice).run();
    val chain = blockchain @ { "chain" };

    // Propose a pause
    alice_proposes_action(chain, blockchain_action.pause);
    assert_true(exists(delayed_event @? { .active, .time == rell.test.last_block_time - rell.test.block_interval + 86400000 }));

    // No delay config
    rell.test.tx().op(
        set_blockchain_config_delay_op(chain, 0)
    ).run();
    // Pause is no longer delayed and will be applied next block
    assert_true(exists(delayed_event @? { .active, .time == rell.test.last_block_time - rell.test.block_interval }));
    rell.test.block().run();
    assert_equals(chain.state, blockchain_state.PAUSED);
    assert_false(exists(delayed_event @? {  }));
}

function test_that_invalid_delayed_proposal_does_break_chain() {
    setup();

    val container = "container";
    val alice = provider @ { rell.test.pubkeys.alice };

    // 1 day delay
    val conf = apply_required_configuration_properties([DIRECTORY_CHAIN_CONFIG_KEY : [DIRECTORY_CHAIN_CONFIG_DELAY_CONFIG_KEY : 86400000].to_gtv()]);
    rell.test.tx().op(
        create_container(alice.pubkey, container, clusters.system, 0, [alice.pubkey]),
        propose_blockchain(alice.pubkey, conf.to_gtv().to_bytes(), "chain", container),
    ).sign(rell.test.keypairs.alice).run();
    val chain = blockchain @ { "chain" };

    // Propose a removal
    alice_proposes_action(chain, blockchain_action.remove);
    assert_true(exists(delayed_event @? { .active, .time == rell.test.last_block_time - rell.test.block_interval + 86400000 }));

    // Add a dependent chain that will prevent removal
    val dependent_conf = apply_required_configuration_properties(["dependencies": [["dep".to_gtv(), chain.rid.to_gtv()]].to_gtv()]);
    rell.test.set_next_block_time_delta(86400000);
    rell.test.tx().op(
        propose_blockchain(alice.pubkey, dependent_conf.to_gtv().to_bytes(), "dependent_chain", container),
    ).sign(rell.test.keypairs.alice).run();

    // Trigger removal action and see that it's not applied but chain is not broken either and delayed action is gone
    rell.test.block().run();
    assert_false(exists(delayed_event @? {}));
    assert_equals(chain.state, blockchain_state.RUNNING);

    val blockchain_remove_proposal = get_blockchain_action_proposal();
    assert_equals(blockchain_remove_proposal.state, proposal_state.FAILED);
}

function test_delayed_proposal_gets_approved() {
    setup();

    val container = "container";
    val alice = provider @ { rell.test.pubkeys.alice };

    // 1 block delay
    val conf = apply_required_configuration_properties([DIRECTORY_CHAIN_CONFIG_KEY : [DIRECTORY_CHAIN_CONFIG_DELAY_CONFIG_KEY : 10000].to_gtv()]);
    rell.test.tx().op(
        create_container(alice.pubkey, container, clusters.system, 0, [alice.pubkey]),
        propose_blockchain(alice.pubkey, conf.to_gtv().to_bytes(), "chain", container),
    ).sign(rell.test.keypairs.alice).run();
    val chain = blockchain @ { "chain" };

    // Propose a removal
    alice_proposes_action(chain, blockchain_action.remove);
    assert_true(exists(delayed_event @? { .active, .time == rell.test.last_block_time - rell.test.block_interval + 10000 }));

    val blockchain_remove_proposal_delayed = get_blockchain_action_proposal();
    assert_equals(blockchain_remove_proposal_delayed.state, proposal_state.DELAYED);

    rell.test.block().run();
    assert_false(exists(delayed_event @? {}));
    assert_equals(chain.state, blockchain_state.REMOVED);

    val blockchain_remove_proposal_approved = get_blockchain_action_proposal();
    assert_equals(blockchain_remove_proposal_approved.state, proposal_state.APPROVED);
}

function alice_proposes_action(bc: blockchain, action: blockchain_action) {
    rell.test.tx().op(
        propose_blockchain_action(rell.test.pubkeys.alice, bc.rid, action)
    ).sign(rell.test.keypairs.alice).run();
}

function bob_proposes_failing_action(bc: blockchain, action: blockchain_action, error: text) {
    rell.test.tx().op(
        if (action == blockchain_action.unarchive)
            propose_blockchain_unarchive_action(rell.test.pubkeys.bob, bc.rid, "container", 0)
        else
            propose_blockchain_action(rell.test.pubkeys.bob, bc.rid, action)
    ).sign(rell.test.keypairs.bob).run_must_fail(error);
}

function assert_action_proposed(action: blockchain_action) {
    assert_equals(
        pending_blockchain_action @ { last_proposal() } .action,
        action
    );
}

function get_blockchain_action_proposal(): proposal {
    return proposal @ {.proposal_type == proposal_type.blockchain_action};
}
