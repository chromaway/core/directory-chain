enum blockchain_action {
    pause,
    resume,
    remove,
    archive,
    unarchive
}

struct unarchive_args {
    destination_container: text;
    final_height: integer;
}

entity pending_blockchain_action {
    key proposal;
    key blockchain;
    action: blockchain_action;
    args: byte_array = x"";
}

@extend(proposal_str)
function proposal_blockchain_action_str(prop: proposal): text? =
    pending_blockchain_action @? { prop } ( "%s:%s:%d".format(prop.proposal_type, .action, prop.rowid) );

@extend(apply_voting_result_handlers) function() = [proposal_type.blockchain_action.name: apply_blockchain_action(*)];
@extend(delete_proposal_handlers) function(): map<text, (proposal) -> unit> = [proposal_type.blockchain_action.name: delete_pending_blockchain_action(*)];
@extend(proposal_delay_time_handlers) function() = [proposal_type.blockchain_action.name: blockchain_action_proposal_delay_time_handler(*)];
@extend(proposal_delay_ids_handlers) function() = [proposal_type.blockchain_action.name: blockchain_action_proposal_delay_ids_handler(*)];

function apply_blockchain_action(proposal) {
    val pba = pending_blockchain_action @? { proposal };
    if (pba == null) return;
    validate_blockchain_action_proposal(pba.blockchain, pba.action);
    when (pba.action) {
        pause -> _apply_pause_blockchain(pba, proposal);
        resume -> _apply_resume_blockchain(pba, proposal);
        remove -> _apply_delete_blockchain(pba, proposal);
        archive -> _apply_archive_blockchain(pba, proposal);
        unarchive -> _apply_unarchive_blockchain(pba, proposal);
    }
}

function delete_pending_blockchain_action(proposal) {
    delete pending_blockchain_action @? { proposal };
}

function blockchain_action_proposal_delay_time_handler(proposal): (blockchain: blockchain?, delay: integer) {
    return blockchain_config_delay_time_handler(pending_blockchain_action @? {
        proposal, .action in [blockchain_action.pause, blockchain_action.remove, blockchain_action.archive]
    } ( .blockchain ));
}

function blockchain_action_proposal_delay_ids_handler(blockchain): list<rowid> {
    return pending_blockchain_action @* {
        blockchain, .action in [blockchain_action.pause, blockchain_action.remove, blockchain_action.archive]
    } ( .proposal.rowid );
}

// Stop block production by pausing
function _apply_pause_blockchain(action: pending_blockchain_action, proposal) {
    action.blockchain.state = blockchain_state.PAUSED;
    set_blockchain_proposal_delay_states(action.blockchain, false);
}

// Restart block production
function _apply_resume_blockchain(action: pending_blockchain_action, proposal) {
    action.blockchain.state = blockchain_state.RUNNING;
    set_blockchain_proposal_delay_states(action.blockchain, true);
}

// Delete everything about this bc except D1 information
function _apply_delete_blockchain(action: pending_blockchain_action, proposal) {
    val bc = action.blockchain;
    validate_blockchain_removal(bc);
    remove_blockchain(bc);
    delete pending_blockchain_action @ { proposal };
}

function _apply_archive_blockchain(action: pending_blockchain_action, proposal) {
    action.blockchain.state = blockchain_state.ARCHIVED;
    val blockchain_cluster = container_blockchain @ { action.blockchain }.container.cluster.name;
    delete blockchain_replica_node @* { action.blockchain };
    create inactive_blockchain(action.blockchain, height = op_context.block_height, timestamp = op_context.last_block_time, blockchain_cluster);
}

function _apply_unarchive_blockchain(action: pending_blockchain_action, proposal) {
    action.blockchain.state = blockchain_state.UNARCHIVING;
    val args = unarchive_args.from_bytes(action.args);

    // keep src cluster/container nodes as replicas
    val src_container = container_blockchain @ { action.blockchain } (.container);
    val src_cluster_nodes_to_replicate_on = cluster_node @* {
        src_container.cluster, .node not in blockchain_replica_node @* { action.blockchain } (.node)
    } (.node);
    for (node in src_cluster_nodes_to_replicate_on) {
        create blockchain_replica_node(action.blockchain, node);
    }
    // remove bc from container
    delete_container_blockchain_by_blockchain(action.blockchain);

    // add bc to the dst container
    val dst_container = require_container(args.destination_container);
    require_container_is_not_full(dst_container);
    create_container_blockchain(dst_container, action.blockchain);
    // remove dst cluster/container nodes from bc replicas
    val dst_nodes = cluster_node @* { dst_container.cluster } (@omit @sort .node.pubkey, .node);
    require(dst_nodes.size() > 0, "Cluster %s must have at least one node".format(dst_container.cluster.name));
    for (node in dst_nodes) {
        delete blockchain_replica_node @* { action.blockchain, node };
    }

    // create a base_config and signers config at final_height (base_config needed if signers don't change)
    // FYI: PCU-based update (i.e. update_configuration_signers_regular()) can't be used here.
    require_height_is_greater_or_equal_to_last_config_height(action.blockchain, args.final_height);
    // base config
    val base_config = require(get_blockchain_configuration(action.blockchain.rid, args.final_height)?.base_config,
        "Can't find config for %s for block %s".format(action.blockchain.rid, args.final_height));
    compress_and_store_configuration(action.blockchain, args.final_height + 1, make_config_unique(base_config));
    // signers config
    val encoded_dst_nodes = (dst_nodes @* {} ( .pubkey )).to_gtv().to_bytes();
    create blockchain_configuration_signers(action.blockchain, args.final_height + 1, encoded_dst_nodes);

    // create unarchiving_blockchain state
    create unarchiving_blockchain(action.blockchain, source = src_container, destination = dst_container, args.final_height);
    delete inactive_blockchain @? { action.blockchain };
}

/**
 * Propose a blockchain action (pause, resume, remove, archive).
 * 
 * Permission: container deployer
 * 
 * Rate limit: actions
 * 
 * @param my_pubkey pubkey of provider
 */
operation propose_blockchain_action(my_pubkey: pubkey, blockchain_rid: byte_array, action: blockchain_action, description: text = "") {
    require(action != blockchain_action.unarchive, "Use propose_blockchain_unarchive_action() for unarchive operation");
    propose_blockchain_action_pubkey_impl(my_pubkey, blockchain_rid, action, x"", description);
}

/**
 * Propose a blockchain action (pause, resume, remove, archive).
 * The transaction needs to include ICCF proof of `tx_to_prove`.
 *
 * Permission: container deployer
 *
 * Rate limit: actions
 *
 * @param tx_to_prove  the transaction from other chain to use for authentication
 * @param op_index     index of the relevant operation in tx_to_prove
 * @param my_pubkey    pubkey of provider
 */
operation propose_blockchain_action_iccf(tx_to_prove: gtx_transaction, op_index: integer, my_pubkey: pubkey) {
    val me = require_provider(my_pubkey);
    val args = require_provider_with_iccf_proof_and_rate_limit(tx_to_prove, op_index,
        op_context.get_current_operation().name, 2, me);
    val blockchain_rid = byte_array.from_gtv(args[0]);
    val action = blockchain_action.from_gtv(args[1]);

    require(action != blockchain_action.unarchive, "Unarchiving via ICCF is not supported");
    val proposal = propose_blockchain_action_impl(me, blockchain_rid, action, x"", iccf_description);
    apply_iccf_proposal(proposal);
}

/**
 * Propose blockchain unarchive action.
 * 
 * Permission: container deployer
 * 
 * Rate limit: actions
 * 
 * @param my_pubkey pubkey of provider
 */
operation propose_blockchain_unarchive_action(my_pubkey: pubkey, blockchain_rid: byte_array, destination_container: text, final_height: integer, description: text = "") {
    val args = unarchive_args(destination_container, final_height);
    propose_blockchain_action_pubkey_impl(my_pubkey, blockchain_rid, blockchain_action.unarchive, args.to_bytes(), description);
}

function propose_blockchain_action_pubkey_impl(my_pubkey: pubkey, blockchain_rid: byte_array, action: blockchain_action, args: byte_array, description: text = "") {
    val me = require_is_provider_with_rate_limit(my_pubkey);
    val prop = propose_blockchain_action_impl(me, blockchain_rid, action, args, description);
    internal_vote(me, prop, true);
}

function propose_blockchain_action_impl(me: provider, blockchain_rid: byte_array, action: blockchain_action, args: byte_array, description: text = ""): proposal {
    // blockchain state requirements
    val blockchain = require_blockchain(blockchain_rid);
    val pending_proposal = pending_blockchain_action @? { blockchain, .proposal.state == proposal_state.PENDING };
    require(empty(pending_proposal), "Blockchain action already proposed: " + pending_proposal?.proposal);

    validate_blockchain_action_proposal(blockchain, action);

    val container = container_blockchain @ { blockchain } .container;
    require_container_deployer(container, me);
    var deployer = container.deployer;

    // action requirements
    if (action == blockchain_action.remove) {
        validate_blockchain_removal(blockchain);
    } else if (action == blockchain_action.archive) {
        require(not(blockchain.system), "Archiving of system chains is not allowed: " + blockchain.rid);
    } else if (action == blockchain_action.unarchive) {
        // unarchiving from a container to another container running on the same node is not allowed
        val args0 = unarchive_args.from_bytes(args);
        val dst_container = require_container(args0.destination_container);
        require_container_deployer(dst_container, me);
        deployer = dst_container.deployer;
        require_container_is_not_full(dst_container);
    }

    val prop = create_proposal(proposal_type.blockchain_action, me, deployer, description);
    create pending_blockchain_action(prop, blockchain, action, args);
    return prop;
}

/**
 * Returns blockchain action proposal.
 */
query get_blockchain_action_proposal(rowid) {
    val proposal = get_latest_proposal(rowid, proposal_type.blockchain_action);
    if (proposal == null) return null;
    val pba = pending_blockchain_action @ { proposal };
    return (
        blockchain = pba.blockchain.rid,
        blockchain_name = pba.blockchain.name,
        action = pba.action
    );
}

/**
 * Returns blockchain unarchive action proposal.
 */
query get_blockchain_unarchive_action_proposal(rowid) {
    val proposal = get_latest_proposal(rowid, proposal_type.blockchain_action);
    if (proposal == null) return null;
    val pba = pending_blockchain_action @ { proposal };
    require(pba.action == blockchain_action.unarchive, "Proposal action is not 'unarchive'. Use get_blockchain_action_proposal() query");
    val args = unarchive_args.from_bytes(pba.args);
    val source_container = container_blockchain @ { pba.blockchain } ( .container.name );
    return (
        blockchain = pba.blockchain.rid,
        blockchain_name = pba.blockchain.name,
        action = pba.action,
        source_container = source_container,
        destination_container = args.destination_container,
        final_height = args.final_height
    );
}

function validate_blockchain_action_proposal(blockchain, action: blockchain_action) {

    // simple states
    when (blockchain.state) {
        blockchain_state.RUNNING -> {
            require(action != blockchain_action.resume, "Blockchain is already running: " + blockchain.rid);
            require(action != blockchain_action.unarchive, "Running blockchain can't be unarchived: " + blockchain.rid);
        }
        blockchain_state.PAUSED -> {
            require(action != blockchain_action.pause, "Blockchain is already paused: " + blockchain.rid);
            require(action != blockchain_action.unarchive, "Paused blockchain can't be unarchived: " + blockchain.rid);
        }
        blockchain_state.REMOVED -> {
            require(false, "Removed blockchain can't be %sd: %s".format(action.name, blockchain.rid));
        }
        blockchain_state.IMPORTING -> {
            require(false, "Importing blockchain can't be %sd: %s".format(action.name, blockchain.rid));
        }
        blockchain_state.ARCHIVED -> {
            require(action != blockchain_action.pause, "Archived blockchain can't be paused: " + blockchain.rid);
            require(action != blockchain_action.resume, "Archived blockchain can't be resumed: " + blockchain.rid);
            require(action != blockchain_action.archive, "Blockchain is already archived: " + blockchain.rid);
        }
    }

    // complex state: moving
    if (exists(pending_blockchain_move @? { blockchain }) or exists(moving_blockchain @? { blockchain, .final_height == -1 })) {
        require(false, "Moving blockchain can't be %sd: %s".format(action.name, blockchain.rid));
    }

    // complex state: unarchiving
    if (exists(unarchiving_blockchain @? { blockchain })) {
        require(false, "Unarchiving blockchain can't be %sd: %s".format(action.name, blockchain.rid));
    }
}

function validate_blockchain_removal(blockchain) {
    require(empty(blockchain_dependency @* { .dependent_on == blockchain }), "Blockchain can't be removed since other blockchains depend on it: " + blockchain.rid);
}
