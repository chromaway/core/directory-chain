entity pending_configuration {
    key proposal;
    blockchain;
    data: byte_array;
}

@extend(apply_voting_result_handlers) function() = [proposal_type.configuration.name: apply_configuration(*)];
@extend(delete_proposal_handlers) function(): map<text, (proposal) -> unit> = [proposal_type.configuration.name: delete_pending_configuration(*)];
@extend(proposal_delay_time_handlers) function() = [proposal_type.configuration.name: configuration_proposal_delay_time_handler(*)];
@extend(scheduled_proposal_delay_time_handlers) function() = [proposal_type.configuration.name: scheduled_configuration_proposal_delay_time_handler(*)];
@extend(proposal_delay_ids_handlers) function() = [proposal_type.configuration.name: configuration_proposal_delay_ids_handler(*)];

/**
 * Handler to enable and return the delay (if any) for this blockchain and this proposal type.
 */
function configuration_proposal_delay_time_handler(proposal): (blockchain: blockchain?, delay: integer) {
    return blockchain_config_delay_time_handler(pending_configuration @? { proposal } ( .blockchain ));
}

function scheduled_configuration_proposal_delay_time_handler(proposal): (blockchain: blockchain?, delay: integer) {
    val scheduled_at_delay = get_scheduled_event_delay(delay_type.PROPOSAL, proposal.rowid.to_integer());
    if (scheduled_at_delay != null) {
        return (blockchain = pending_configuration @? { proposal } ( .blockchain ), delay = scheduled_at_delay);
    }
    return (blockchain = null, delay = 0);
}

/**
 * Handler to return all active proposals for blockchain with delay
 */
function configuration_proposal_delay_ids_handler(blockchain): list<rowid> {
    return pending_configuration @* { blockchain } ( .proposal.rowid );
}

function apply_configuration(proposal) {
    val pc = pending_configuration @? { proposal };
    if (pc == null) return;
    val is_chain0 = pc.blockchain.rid == chain_context.blockchain_rid;

    if (is_chain0) {
        apply_configuration_chain0(pc);
    } else {
        apply_configuration_regular(pc);
    }
}

function delete_pending_configuration(proposal) {
    delete pending_configuration @? { proposal };
}

function apply_configuration_chain0(pc: pending_configuration) {
    val apply_at_height = op_context.block_height + 1; // NB: compute_blockchain_info_list()/get_cluster_node_blockchains() relies on this
    log("Base configuration update chain0 at height %d".format(apply_at_height));
    require(empty(blockchain_configuration @? { pc.blockchain, apply_at_height }), "Configuration at height %d already exists".format(apply_at_height));
    val unique_base_config = make_config_unique(pc.data);
    compress_and_store_configuration(pc.blockchain, apply_at_height, unique_base_config);
    add_dependencies(unique_base_config, pc.blockchain.rid, apply_at_height);
}

function apply_configuration_regular(pc: pending_configuration) {
    val last_configuration_height = (blockchain_configuration @? { pc.blockchain } (@sort_desc .height) limit 1) ?: 0;
    val last_pending_configuration = pending_blockchain_configuration @? { pc.blockchain } (@sort_desc @omit .minimum_height, $) limit 1;
    val (minimum_height, signers) = if (last_pending_configuration != null)
        (last_configuration_height.max(last_pending_configuration.minimum_height) + 1,
         last_pending_configuration.signers)
     else
        (last_configuration_height + 1,
         blockchain_configuration_signers @ { pc.blockchain } (@omit @sort_desc .height, .signers) limit 1);
    log("Base configuration update for chain %s at minimum height %d".format(pc.blockchain.rid, minimum_height));
    val unique_base_config = make_config_unique(pc.data);
    val config_hash = calculate_configuration_hash(unique_base_config, list<byte_array>.from_gtv(gtv.from_bytes(signers)));
    create pending_blockchain_configuration(
        pc.blockchain,
        minimum_height,
        config_hash,
        base_config = unique_base_config,
        signers
    );
    create blockchain_configuration_update_attempt(
        pc.proposal,
        config_hash = config_hash,
        blockchain_rid = pc.blockchain.rid,
    );
}

/**
 * Propose new blockchain configuration.
 *
 * Permission: container deployer
 *
 * Rate limit: actions
 *
 * @param my_pubkey pubkey of provider
 * @param scheduled_at The time to apply this proposal when approved. If the proposal is approved after the time has passed the proposal is applied right away.
 */
operation propose_configuration(my_pubkey: pubkey, blockchain_rid: byte_array, config_data: byte_array, description: text = "", scheduled_at: timestamp? = null) {
    val me = require_is_provider_with_rate_limit(my_pubkey);
    val proposal = create_configuration_proposal(me, blockchain_rid, config_data, description, scheduled_at);
    internal_vote(me, proposal, true);
}

/**
 * Propose new blockchain configuration.
 * The transaction needs to include ICCF proof of `tx_to_prove`.
 *
 * Permission: container deployer
 *
 * Rate limit: actions
 *
 * @param tx_to_prove  the transaction from other chain to use for authentication
 * @param op_index     index of the relevant operation in tx_to_prove
 * @param my_pubkey    pubkey of provider
 */
operation propose_configuration_iccf(tx_to_prove: gtx_transaction, op_index: integer, my_pubkey: pubkey) {
    val me = require_provider(my_pubkey);
    val args = require_provider_with_iccf_proof_and_rate_limit(tx_to_prove, op_index,
        op_context.get_current_operation().name, 2, me);
    val blockchain_rid = byte_array.from_gtv(args[0]);
    val config_data = byte_array.from_gtv(args[1]);

    val proposal = create_configuration_proposal(me, blockchain_rid, config_data, iccf_description);
    apply_iccf_proposal(proposal);
}

function create_configuration_proposal(provider, blockchain_rid: byte_array, config_data: byte_array, description: text, scheduled_at: timestamp? = null): proposal {
    val blockchain = require_active_blockchain(blockchain_rid);
    val container = container_blockchain @ { blockchain } .container;
    require_container_deployer(container, provider);
    val is_chain0 = blockchain.rid == chain_context.blockchain_rid;

    validate_blockchain_configuration(blockchain, container, config_data, signers = not is_chain0, header_hash = not is_chain0, system_chain = blockchain.system);

    val prop = create_proposal(proposal_type.configuration, provider, container.deployer, description);
    if (scheduled_at != null) {
        require(scheduled_at > op_context.last_block_time, "The scheduled time must be greater than last block at %d".format(op_context.last_block_time));
        add_scheduled_event(delay_type.PROPOSAL, prop.rowid.to_integer(), scheduled_at);
    }
    create pending_configuration(prop, blockchain, config_data);
    return prop;
}

struct blockchain_configuration_data {
    height: integer;
    blockchain;
    data: byte_array;
}

struct pending_blockchain_configuration_data {
    proposal;
    blockchain;
    data: byte_array;
}

/**
 * Returns blockchain configuration proposal.
 *
 * @deprecated use get_configuration_proposal_v64 instead
 */
query get_configuration_proposal(rowid?): (
    current_conf:blockchain_configuration_data,
    proposed_conf:pending_blockchain_configuration_data
)? {
    val proposal = get_latest_proposal(rowid, proposal_type.configuration);
    if (proposal == null) return null;
    val config = pending_configuration @? { require_proposal(proposal.rowid) }
        (pending_blockchain_configuration_data(.proposal, .blockchain, decompress_configuration(.data)));
    if (config == null) return null;

    val current = get_latest_blockchain_configuration_data(config.blockchain);

    return (
        current_conf = blockchain_configuration_data(current.height, config.blockchain, current.config),
        proposed_conf = config
    );
}

struct configuration_proposal_data {
    blockchain_rid: byte_array;
    current_height: integer;
    current_configuration: byte_array;
    proposed_configuration: byte_array;
}

/**
 * Returns blockchain configuration proposal.
 */
query get_configuration_proposal_v64(rowid?): configuration_proposal_data? {
    val proposal = get_latest_proposal(rowid, proposal_type.configuration);
    if (proposal == null) return null;
    val proposal_details = pending_configuration @? { require_proposal(proposal.rowid) }
        (.blockchain, config = decompress_configuration(.data));
    if (proposal_details == null) return null;

    val current = get_latest_blockchain_configuration_data(proposal_details.blockchain);

    return configuration_proposal_data(
        blockchain_rid = proposal_details.blockchain.rid,
        current_height = current.height,
        current_configuration = current.config,
        proposed_configuration = proposal_details.config,
    );
}
